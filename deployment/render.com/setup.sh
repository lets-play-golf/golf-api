###
### Setup render.com deployment folder 
###
### @see: https://www.freecodecamp.org/news/how-to-deploy-nodejs-application-with-render/
###
### - Render.com is configured to use a git hook pointing at our gitlab repo and branch to trigger a deployment when a commit is pushed
### - This script recreates the target folder from scratch; and
### - clones the git repo one folder up with the `targetFolder` name
###

targetFolder=../deploy-to-render
targetBranch=main
gitRepoUrl=git@gitlab.com:lets-play-golf/golf-deploy-render.com.git

# Recreate clean folder
rm -rf $targetFolder
mkdir $targetFolder
cd $targetFolder

# Clone git repo
git init
git remote add origin $gitRepoUrl
git pull origin $targetBranch 
git checkout $targetBranch

## Add gitignore - only when git repo is used for the first time
echo "/client" > ./.gitignore