###
### Deploy to render.com: Downloads api and ui projects and deploys branches to render app
### - back-end: the app is set up to accept graphql post requests.
### - front-end: the app is set up to serve files from the `client/build` folder
###

# parameter 1: branch to checkout, defaulted to master
branch=${1:-master}
echo "Deploying $branch to render.com"
 
targetFolder=deploy-to-render
targetBranch=main

###
### 1. Download the golf-api and golf-ui projects from the remote gitlab repos:
###
### targetFolder
### ├── client: copy golf-ui content here, then run the build script
### |   └── build: contains the production build output
### └── server: copy golf-api content here.
###
rm -rf ../$targetFolder/client
rm -rf ../$targetFolder/server

git clone -b $branch git@gitlab.com:lets-play-golf/golf-api.git ../$targetFolder/server
git clone -b $branch git@gitlab.com:lets-play-golf/golf-ui.git ../$targetFolder/client

lastServerCommit="$(git -C ../$targetFolder/server log -1 --pretty=%B)"
lastClientCommit="$(git -C ../$targetFolder/client log -1 --pretty=%B)"

rm -rf ../$targetFolder/client/.git
rm -rf ../$targetFolder/server/.git
rm -rf ../$targetFolder/server/.gitignore

### 
### - Copy the content of client/build folder to the server folder
###
### targetFolder
### ├── client: copy golf-ui content here, then run the build script
### └── server: copy golf-api content here.
###

# Install and build react app, then copies production build into the server project

yarn --cwd ../$targetFolder/client install --production
yarn --cwd ../$targetFolder/client build

cp -R ../$targetFolder/client/build ../$targetFolder/server/public-files

# Commit and push to our gitlab repo, this will trigger the hook on render.com ad kick off a deployment
git -C ../$targetFolder add server/.
git -C ../$targetFolder commit -am "Synchronise client and server changes from branch: $branch
# last commits:
# - golf-api: ${lastServerCommit}
# - golf-ui: ${lastClientCommit}
# "

git -C ../$targetFolder push --set-upstream origin $targetBranch