# Heroku deploy: Downloads api and ui projects and deploys branches to heroku app
# parameter 1: branch to checkout, defaulted to master
branch=${1:-master}
path=lets-play-golf-deploy-heroku

echo "Deploying $branch to heroku"

rm -rf ../../$path/client
rm -rf ../../$path/server

# Download branch from git
git clone -b $branch git@gitlab.com:lets-play-golf/golf-api.git ../../$path/server
git clone -b $branch git@gitlab.com:lets-play-golf/golf-ui.git ../../$path/client
rm -rf ../../$path/*/.git*

# Install and build react app, then copies production build into the server project
cd ../../$path/client
yarn install --production
yarn build
cd ../server
pwd
cp -R client/build server/public-files
pwd
cd ..
ls -ltr

# Commit and push to heroku, initiates the build and deploy on heroku's servers
git add server/.
git commit -am "Synchronise client and server changes"
git push --set-upstream heroku master
