# Heroku Setup: creates a deployment folder outside of this project and adds heroku remote
appname=lets-play-golf

## Recreate clean folder
rm -rf ../$appname-deploy-heroku
mkdir ../$appname-deploy-heroku
cd ../$appname-deploy-heroku

## Add heroku git remote
git init
heroku git:remote -a $appname
git pull heroku master

## Add gitignore
echo "/client" > ../$appname-deploy-heroku/.gitignore
