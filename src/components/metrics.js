const client = require('prom-client')

const histograms = {
  mongoOperation: new client.Histogram({
    name: 'golf_api_mongodb_operation',
    help: 'Time in ms to complete a mongodb operation',
    buckets: [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000],
    labelNames: ['operation', 'collection', 'result'],
  }),
  graphqlOperation: new client.Histogram({
    name: 'golf_api_graphql_operation',
    help: 'Time in ms to complete a graphql operation',
    buckets: [20, 50, 100, 200, 500, 1000, 1500, 2000, 5000, 10000],
    labelNames: ['operation', 'result'],
  }),
}

/**
 * Records a metric reading
 * @param {object} options
 * @param {object} options.histogram: histogram object as defined above
 * @param {number} options.value: the number value for the reading, e.g. request timing
 * @param {object} options.labels: extra labels associated with this reading
 * @returns
 */
const observe = ({ histogram, value, labels = {} }) => {
  if (isNaN(value))
    throw new Error(`Cannot observe metric without a number value`)

  try {
    return histogram.observe(labels, value)
  } catch (error) {
    throw new Error(
      `Failed to observe metric ${histogram.name}. Error is: ${error}`
    )
  }
}

module.exports = {
  name: 'metrics',

  observeMongoOperation({ collection, operation, timeInMs, isError = false }) {
    const result = isError ? 'error' : 'success'
    process.env.NODE_ENV !== 'test' &&
      console.log(
        'observeMongoOperation',
        operation,
        collection,
        timeInMs,
        'ms',
        result
      )
    observe({
      histogram: histograms.mongoOperation,
      value: timeInMs,
      labels: {
        collection,
        operation,
        result: isError ? 'error' : 'success',
      },
    })
  },

  observeGraphqlOperation({ operation, timeInMs, isError = false }) {
    const result = isError ? 'error' : 'success'
    process.env.NODE_ENV !== 'test' &&
      console.log('observeGraphqlOperation', operation, timeInMs, 'ms', result)
    observe({
      histogram: histograms.graphqlOperation,
      value: timeInMs,
      labels: {
        operation,
        result: isError ? 'error' : 'success',
      },
    })
  },
}
