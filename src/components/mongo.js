const { MongoClient } = require('mongodb')

module.exports = {
  name: 'mongo',
  deps: ['metrics'],

  async init() {
    await this.connect()
  },

  async connect() {
    const url = this.getConnectionUrl(this.config)
    try {
      this.client = await MongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      this.db = this.client.db(this.config.database)
      this.isConnected = true
      this.attempts = 1
      if (!['local', 'test'].includes(process.env.NODE_ENV))
        this.client.once('close', this.connect)
    } catch (err) {
      console.log(`Mongo error connecting to ${url}`, err)
      this.isConnected = false
      err.attempts = this.attempts
      this.attempts++
      await new Promise((resolve) => setTimeout(resolve, 2000))
      return this.connect()
    }
  },

  getConnectionUrl(config) {
    if (config.url) return config.url
    return this.createUrl(config)
  },

  createUrl(config) {
    const {
      username,
      password,
      hosts = [],
      database = '',
      isCluster = false,
    } = config
    const stringifiedHosts = hosts
      .map(({ host, port = 27017 }) => `${host}:${port}`)
      .join(',')
    const auth = username ? `${username}${password ? `:${password}@` : ''}` : ''
    return `mongodb${
      isCluster ? '+srv' : ''
    }://${auth}${stringifiedHosts}/${database}`
  },

  /**
   * Runs operation against mongo collection and tracks the request timing
   * @param {string} options.collection: the name of the collection to run the operation against
   * @param {string} options.operation: the operation function run against the collection
   * @param {string} options.fn: function taking the db.collection('collection') and returning the result, to run.
   */
  async runOperation({ collection, fn, operation }) {
    if (!collection) throw Error('Cannot run operation without collection')
    if (!fn) throw Error('Cannot run operation without fn')
    if (!operation) throw Error('Cannot run operation without fn')

    const start = new Date()

    try {
      const result = await fn(this.db.collection(collection))
      this.metrics.observeMongoOperation({
        collection,
        operation,
        timeInMs: new Date() - start,
      })
      return result
    } catch (e) {
      this.metrics.observeMongoOperation({
        collection,
        operation,
        timeInMs: new Date() - start,
        isError: true,
      })
      throw e
    }
  },
}
