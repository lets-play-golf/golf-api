const { ObjectId } = require('mongodb')
const _ = require('lodash')

module.exports = {
  name: 'resourceModel',
  deps: ['mongo'],

  /**
   * Create new document in mongo collection
   * @param {string} options.collection: required - mongo collection where document is to be created
   * @param {object} options.obj: required - document to be created
   * @param {object} options.user: required - user creating the document - or pass null if the gateway is creating it.
   * @param {function} options.formatter: optional - function formatting returned document
   */
  async create({ collection, obj, formatter, user }) {
    if (!collection) throw Error('resourceModel error: no collection specified')
    if (!obj)
      throw Error(
        `resourceModel error: no obj specified to create ${collection}`
      )
    if (!user === undefined)
      throw Error(
        `resourceModel error: Missing user to create ${collection}, pass null if not applicable`
      )

    obj = {
      ...obj,
      createdAt: new Date(),
      createdBy: user === null ? undefined : user.id,
      deleted: false,
    }

    const {
      ops: [newObject],
    } = await this.mongo.runOperation({
      collection,
      fn: (coll) => coll.insertOne(obj),
      operation: 'insertOne',
    })

    return this.format(newObject, formatter)
  },

  /**
   * Query documents in mongo
   * @param {string} options.collection: required - queried mongo collection
   * @param {object} options.query: required - property filters
   * @param {number} options.limit: number of documents requested
   * @param {function} options.formatter: optional - function formatting returned document
   */
  async get({
    collection,
    query: { id, ids, ...params } = {},
    limit = 0,
    formatter,
    sort = { _id: -1 },
  }) {
    if (!collection) throw Error('resourceModel error: no collection specified')

    const idsfilter =
      id || ids
        ? { $in: (ids || [id]).map((id) => new ObjectId(id)) }
        : undefined

    const mongoQuery = _.omitBy(
      {
        _id: idsfilter,
        ...params,
        deleted: false,
      },
      _.isUndefined
    )

    const res = await this.mongo.runOperation({
      collection,
      fn: (coll) => coll.find(mongoQuery).limit(limit).sort(sort).toArray(),
      operation: 'find',
    })

    return res.map((itm) => this.format(itm, formatter))
  },

  /**
   * Update mongo documents
   * @param {string} options.collection: required - queried mongo collection
   * @param {string} options.id: required - id of the document to be updated
   * @param {object} options.update: required - properties updates to be applied to the document
   * @param {function} options.formatter: optional - function formatting returned document
   */
  async update({ collection, id, update, formatter }) {
    if (!collection) throw Error('resourceModel error: no collection specified')
    if (!update) throw new Error('Update object must be provided')
    if (!id) throw new Error("Can't perform an update without id or ids filter")

    update = {
      ...update,
      updatedAt: new Date(),
    }

    const query = {
      _id: new ObjectId(id),
      deleted: false,
    }

    const { value } = await this.mongo.runOperation({
      collection,
      fn: (coll) =>
        coll.findOneAndUpdate(
          _.omitBy(query, _.isUndefined),
          { $set: _.omitBy(update, _.isUndefined) },
          { returnOriginal: false }
        ),
      operation: 'findOneAndUpdate',
    })
    return this.format(value, formatter)
  },

  /**
   * Soft delete mongo objects - mark as deleted: true
   * @param {string} collection
   * @param {string} id
   */
  async delete({ collection, id, formatter }) {
    if (!collection) throw Error('resourceModel error: no collection specified')
    if (!id) throw new Error("Can't perform a delete without id")

    return this.update({ collection, id, update: { deleted: true }, formatter })
  },

  /**
   * Update an array using mongo $push
   * @param {*} options.collection: required - mongo collection name
   * @param {*} options.query: require - query including document id
   * @param {*} options.obj: object to push in the format { `field`: `object` }
   */
  async pushUpdate({ collection, query: { id }, obj, formatter }) {
    if (!collection)
      throw Error('resourceModel $pushUpdate error: no collection specified')
    if (!id) throw Error('resourceModel $pushUpdate error: missing document id')
    if (!obj)
      throw Error('resourceModel $pushUpdate error: missing obj to push')

    const query = {
      _id: new ObjectId(id),
      deleted: false,
    }

    const { value } = await this.mongo.runOperation({
      collection,
      fn: (coll) =>
        coll.findOneAndUpdate(
          _.omitBy(query, _.isUndefined),
          { $push: obj },
          { returnOriginal: false }
        ),
      operation: 'findOneAndUpdate',
    })
    return this.format(value, formatter)
  },

  /**
   * Applies default object formats and formatter function passed by the resource's model
   * Default formats: turn `_id` to `id` and turn ObjectId to string
   * @param {object} obj: the resource object from mongo
   * @param {function} formatter: optional - a function taking the mongo object and returning the object in the desired format
   */

  format(obj, formatter = (itm) => itm) {
    obj = this.defaultFormat(obj)
    return formatter(obj)
  },

  defaultFormat(obj) {
    return Object.entries(obj || {}).reduce((acc, [key, value]) => {
      if (key === '_id') key = 'id'
      if (value instanceof ObjectId) value = value.toString()
      return { ...acc, [key]: value }
    }, {})
  },
}
