module.exports = {
  name: 'sessions',
  deps: ['sessionsModel', 'graphql'],
  CONNECTION_CHANNEL: 'CONNECTION_CHANNEL',

  init() {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('sessions.graphql'),
      resolvers: {
        Query: {
          session: this.getSession,
        },
        Mutation: {
          createSession: this.createSession,
          deleteSession: this.deleteSession,
        },
        SessionResult: {
          __resolveType: (obj) => (obj.error ? 'UserInputError' : 'Session'),
        },
      },
    })
  },

  async getSession(parent, args, { req }) {
    return this.sessionsModel.get({ req })
  },

  async createSession(parent, args, { res }) {
    return this.sessionsModel.create({ ...args, res })
  },

  async deleteSession(parent, args, { res }) {
    return this.sessionsModel.delete({ res })
  },
}
