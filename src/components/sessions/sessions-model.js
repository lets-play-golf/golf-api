const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = {
  name: 'sessionsModel',
  deps: ['usersModel', 'modelRegistry'],

  init() {
    this.modelRegistry.register('sessions', this)
    this.config.allowedFailedAttempts = 3
    this.config.attemptsTimespan = 15 * 60 * 1000
  },

  /**
   * Authenticates user and sets new session token response cookie
   * @param {object} options.email: user email
   * @param {object} options.password: user password
   * @returns {User}: if the user passwords match, returns the User information, otherwise return undefined.
   */
  async create({ email, password, res }) {
    const [userFound] = await this.usersModel.get({ email })
    if (!userFound) return this.wrongCredentials()
    const { hash, ...user } = userFound

    const { accountLock, failedAttempts } =
      await this.modelRegistry.userActivity.getAccountLock({ user })
    if (accountLock) return this.accountLocked({ accountLock, user })

    const match = await bcrypt.compareSync(password, hash || 'undefined')
    if (!match) return this.handleFailedAttempt({ failedAttempts, user })

    this.setCookie(user, res)
    return {
      user,
    }
  },

  async handleFailedAttempt({ user }) {
    // Record failedAttempt
    await this.modelRegistry.userActivity.create({ type: 'LoginFailed', user })
    const { accountLock } = this.modelRegistry.userActivity.getAccountLock({
      user,
    })
    if (accountLock) return this.accountLocked({ accountLock, user })
    return this.wrongCredentials()
  },

  // Errors
  wrongCredentials() {
    return this.utilities.UserInputError(
      'Username or password is incorrect',
      'WRONG_CREDENTIALS'
    )
  },
  async accountLocked({ accountLock, user }) {
    await this.modelRegistry.userActivity.create({
      type: 'AccountLocked',
      user,
      timeInMins: Math.ceil((accountLock - new Date()) / 1000 / 60),
    })
    return this.utilities.UserInputError(
      `You have entered your password wrong too many times, your account is now locked for ${Math.ceil(
        (accountLock - new Date()) / 1000 / 60
      )} minutes.`,
      'ACCOUNT_LOCKED',
      [],
      { time: Math.ceil((accountLock - new Date()) / 1000 / 60) }
    )
  },

  /**
   * Decodes session information from the token
   * @param {*} options.req: Incoming server request object
   * @param {*} options.wsReq: Incoming server request object for ws connection
   */
  get({ req, wsReq, res }) {
    const debug = false
    try {
      const token = (() => {
        if (req) return req.cookies[this.config.cookie.name]
        return wsReq.headers.cookie
          .split('; ')
          .filter((itm) => itm.startsWith(this.config.cookie.name))[0]
          .split('=')[1]
      })()
      // eslint-disable-next-line no-unused-vars
      const { iat, ...user } = this.decodeToken(token)
      if (req && res) this.setCookie(user, res)
      return { user }
    } catch (e) {
      if (debug && req && e.message === 'jwt must be provided')
        return console.log('No session cookie on the request')
      if (
        debug &&
        wsReq &&
        e.message === "Cannot read property 'split' of undefined"
      )
        return console.log('No session cookie on the request')
      if (debug && e.message === 'jwt malformed')
        return console.log('Token was tampered with')
      return {}
    }
  },

  /**
   * Removes the session token cookie from the response
   * @param {*} options.res: Server response object
   */
  async delete({ res }) {
    res.clearCookie(this.config.cookie.name, this.config.token.cookiePolicy)
    return true
  },

  setCookie(user, res) {
    const token = this.createToken(user)
    const { name, lifespan, options } = this.config.cookie
    const cookieOptions = {
      expires: new Date(Date.now() + lifespan),
      ...options,
    }
    res.cookie(name, token, cookieOptions)
  },

  createToken({ id, email, firstname, username, isAdmin = false }) {
    return jwt.sign(
      { id, email, firstname, username, isAdmin },
      this.config.token.encryptionKey
    )
  },

  decodeToken(token) {
    return jwt.verify(token, this.config.token.encryptionKey)
  },

  getFromContext({ user }) {
    return { user }
  },
}
