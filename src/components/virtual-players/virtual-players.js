const DELAY_IN_MS = 1600

/**
 * Generic VirtualPlayer handling responses to chat messages and game updates
 */
class VirtualPlayer {
  constructor({ user, golf }) {
    this.user = user
    this.golf = golf
  }

  async delay() {
    return new Promise((resolve) => setTimeout(resolve, DELAY_IN_MS))
  }

  availableSpots(golf) {
    return golf.players
      .find((itm) => itm.id === this.user.id)
      .cards.map((itm, index) => itm === null && index)
      .filter(Number.isInteger)
  }

  randomValue(array) {
    return array[Math.floor(Math.random() * array.length)]
  }
}

class Randobot extends VirtualPlayer {
  constructor({ golf, user }) {
    super({
      user,
      golf,
    })
  }

  /**
   * Returns the virtual player's next move after receiving a gameUpdate
   */
  async onGameUpdateReceived(gameUpdated) {
    await this.delay()
    // Exit if peekCards or not the bot's turn to play
    if (!gameUpdated.events) return
    const nextTurn = this.golf.NextTurn.get(gameUpdated)
    const { actions, players } = nextTurn
    if (!players.includes(this.user.id)) return

    // Randobot randomly selects an action
    const action = this.randomValue(actions)

    // Run function corresponding to the action
    if (!this[action])
      throw new Error(
        `randobot could not find function corresponding to the action: ${action} `
      )
    return this[action](gameUpdated, nextTurn)
  }

  /**
   * Randobot's own personal response
   * @param {*} message
   * @returns
   */
  async onMessageReceived({ messageSent }) {
    await this.delay()
    if (messageSent?.message.includes('?'))
      return `You tell me, I'm just a bot!`
    if (messageSent?.message.includes('*')) return `Language!`
    return `Hello, I'm ${this.user.username}`
  }

  start() {
    return { action: 'start' }
  }

  pick_cards() {
    return {
      action: 'pick_cards',
      spots: [this.randomValue([0, 1]), this.randomValue([2, 3])],
    }
  }

  pickup() {
    return { action: 'pickup', pile: this.randomValue(['PICKUP', 'DISCARD']) }
  }

  place(gameUpdated) {
    const golf = new this.golf.Golf(gameUpdated).getPublicView()
    const spot = this.randomValue(this.availableSpots(golf))
    return { action: 'place', spot }
  }

  reveal(gameUpdated) {
    const golf = new this.golf.Golf(gameUpdated).getPublicView()
    const spot = this.randomValue(this.availableSpots(golf))
    return { action: 'reveal', spot }
  }

  discard() {
    return { action: 'discard' }
  }
}

module.exports = {
  name: 'virtualPlayers',
  deps: ['modelRegistry', 'golf'],

  Randobot,
}
