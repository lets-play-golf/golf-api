const UNSUNBSCRIBE_AFTER_IDLE_DELAY_IN_MS = 10000

module.exports = {
  name: 'virtualPlayersSubscriptions',
  deps: [
    'pubsub',
    'chatsSubscriptions',
    'modelRegistry',
    'golf',
    'virtualPlayersModel',
  ],

  /**
   * Entry point to subscribe a virtual-player to game and chat channels so that they can respond to subscription messages
   * - Run this function when a user subscribes to the game channel.
   * - This will check the virtual-players registered on this game and subscribe them.
   */
  async handleVirtualPlayersSubscriptions({ game }) {
    const virtualPlayers = this.virtualPlayersModel.get({
      ids: game.players.map(({ user }) => user),
    })
    virtualPlayers.forEach((user) => {
      this.subscribeChat({ game, user })
      this.subscribeGame({ game, user })
    })
  },

  /**
   * Handles virtual user subscription to the chat channel
   */
  async subscribeChat({ game: { chat }, user }) {
    const chatChannel = this.chatsSubscriptions.channel({
      id: chat,
      user: user.id,
    })

    if (this.pubsub.hasSubscribers(chatChannel)) return
    const subId = await this.pubsub.pubsub.subscribe(chatChannel, (message) =>
      this.onMessageReceived({ message, user, chat })
    )
    // Handle unsubscribe
    const channelStartsWith = chatChannel.split('_').slice(0, -1).join('_')
    this.handleUnsubscribe({ channelStartsWith, subId })
  },

  /**
   * Gets the response from the bot and publishes message if applicable
   */
  async onMessageReceived({ chat, message, user }) {
    // Ignore VP's own messages
    if (message?.messageSent?.from === user.id) return

    const vp = this.virtualPlayersModel.getVP({ id: user.id })
    const responseMessage = await vp.onMessageReceived(message)
    if (!responseMessage) return
    this.executeAction(() =>
      this.modelRegistry.chats.sendMessage({
        id: chat,
        message: responseMessage,
        user,
      })
    )
  },

  async subscribeGame({ game, game: { id }, user }) {
    const gameChannel = `GAME_CHANNEL_${id}_${user.id}`

    // Execute onGameUpdateReceived in case it is the vp's turn to play
    this.onGameUpdateReceived({ message: { gameUpdated: game }, user, game })

    // If already subscribed, do nothing
    if (this.pubsub.hasSubscribers(gameChannel)) return

    const subId = await this.pubsub.pubsub.subscribe(gameChannel, (message) =>
      this.onGameUpdateReceived({ message, user, game })
    )
    // Handle unsubscribe
    const channelStartsWith = gameChannel.split('_').slice(0, -1).join('_')
    this.handleUnsubscribe({ channelStartsWith, subId })
  },

  /**
   * Instantiates the bot and handles their response to the gameUpdate event
   */
  async onGameUpdateReceived({ message, user, game }) {
    const vp = this.virtualPlayersModel.getVP({ id: user.id, game })
    const gameEvent = await vp.onGameUpdateReceived(message.gameUpdated)
    if (!gameEvent) return

    this.executeAction(() =>
      this.modelRegistry.playGame.addEvent({
        id: game.id,
        event: gameEvent,
        user: user,
      })
    )
  },

  /**
   * Unsubscribes the virtual user when it realises there is only one subscriber to the channel matching the pattern
   */
  handleUnsubscribe({ channelStartsWith, subId }) {
    setTimeout(() => {
      const matchingChannels = this.pubsub.matchingChannels(channelStartsWith)
      if (matchingChannels.length > 1)
        return this.handleUnsubscribe({ channelStartsWith, subId })
      this.pubsub.pubsub.unsubscribe(subId)
    }, UNSUNBSCRIBE_AFTER_IDLE_DELAY_IN_MS)
  },

  /**
   * Wraps the callback in a try/catch to prevent the app from crashing
   * @param {function} callback
   */
  async executeAction(callback) {
    try {
      // this await is important to catch a promise rejection, otherwise an error will cause the app to crash
      await callback()
    } catch (e) {
      console.log('bot tried to execute an action but there was an error', e)
    }
  },
}
