jest.useFakeTimers()
const { createComponent } = require('madhouse')
const {
  playGames: [playGameFixture],
} = require('../../../tests/fixtures/games')
const { Randobot } = createComponent(require('./virtual-players'))
const golf = createComponent(require('../golf/golf'))

const mockSendMessage = jest.fn()
const mockedAddEvent = jest.fn()

const vp = {
  golf,
  modelRegistry: {
    chats: { sendMessage: mockSendMessage },
    playGame: { addEvent: mockedAddEvent },
  },
}

// This is to avoid getting logging messages in the test
jest.spyOn(console, 'log').mockImplementation(() => {})

describe('VirtualPlayer', () => {
  afterEach(() => jest.clearAllMocks())

  describe('handleChatMessageReceived', () => {
    it('should respond by publishing a message in the chat', () => {
      new Randobot({
        vp,
        playGameFixture,
      }).handleChatMessageReceived('10101', {
        messageSent: { message: 'hello' },
      })
      jest.runAllTimers()
      expect(mockSendMessage).toHaveBeenCalledTimes(1)
      expect(mockSendMessage).toHaveBeenCalledWith({
        id: '10101',
        message: "Hello, I'm randobot",
        user: { id: '6318b28aea4bd4003c0b7e91', username: 'randobot' },
      })
    })

    it('should respond by publishing a message in the chat', () => {
      new Randobot({
        vp,
        playGameFixture,
      }).handleChatMessageReceived('10101', {
        messageSent: { message: 'How are you?' },
      })
      jest.runAllTimers()
      expect(mockSendMessage).toHaveBeenCalledTimes(1)
      expect(mockSendMessage).toHaveBeenCalledWith({
        id: '10101',
        message: "You tell me, I'm just a bot!",
        user: { id: '6318b28aea4bd4003c0b7e91', username: 'randobot' },
      })
    })
  })
})
