const { Randobot } = require('./virtual-players')

const RANDOBOT = {
  id: '6318b28aea4bd4003c0b7e91',
  username: 'randobot',
}

module.exports = {
  name: 'virtualPlayersModel',
  deps: ['modelRegistry', 'golf'],

  VIRTUAL_PLAYERS: [RANDOBOT],

  /**
   * @returns {boolean} wether the user is a virtual player
   */
  isVirtualPlayer(id) {
    return this.VIRTUAL_PLAYERS.find((vp) => vp.id === id)
  },

  /**
   * Exposes VP user info
   */
  get({ id, ids = [], username }) {
    return this.VIRTUAL_PLAYERS.filter(
      (vp) => ids.concat(id).includes(vp.id) || username === vp.username
    )
  },

  /**
   * Exposes VirtualPlayers instances
   * @param {*} options.id: id of the virtual player
   * @param {*} options.game: Game instance
   * @returns
   */
  getVP({ id }) {
    if (this.get({ id })[0].username === 'randobot')
      return new Randobot({
        user: RANDOBOT,
        golf: this.golf,
      })
    throw new Error(`could not find virtual player with id: ${id}`)
  },

  Randobot,
}
