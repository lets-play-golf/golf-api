/**
 * @type {Player}: The public part of user's information: username, stats, credits, etc.
 */

const GAMES_CREDIT_LIMIT = 5
const { ObjectId } = require('mongodb')

module.exports = {
  name: 'playersModel',
  deps: ['modelRegistry', 'mongo'],
  collection: 'users',
  GAMES_CREDIT_LIMIT,

  async init() {
    this.modelRegistry.register('players', this)
  },

  async get({ id }) {
    if (id) return this.getOnePlayer(id)
    const stats = await this.statsFromAggregation()
    if (!id) return stats
    return stats.find((itm) => itm.id === id)
  },

  // Defaults stats to empty object if player has not completed 3 games yet
  async getOnePlayer(id) {
    const [player] = await this.modelRegistry.users.get({ id })
    const stats = await this.statsFromAggregation()
    const statsPlayer = stats.find((itm) => itm.id === id)
    if (statsPlayer) return statsPlayer
    return { ...player, stats: {}, badges: [] }
  },

  async statsFromAggregation() {
    const players = (
      await this.mongo.runOperation({
        collection: 'users',
        fn: async (coll) =>
          (await coll.aggregate(this.PLAYERS_STATS_AGGREGATION())).toArray(),
        operation: 'aggregation',
      })
    )
      // TODO: Move rating to aggregation
      .map((itm) => ({
        ...itm,
        stats: { ...itm.stats, rating: this.getRating(itm.stats.averageScore) },
      }))

      // TODO: Move sorting to aggregation
      .sort((a, b) => b.stats.rating - a.stats.rating)
    return this.appendBadges(players)
  },

  async gamesFromAggregation({ id } = {}) {
    return this.mongo.runOperation({
      collection: 'users',
      fn: async (coll) =>
        (
          await (
            await coll.aggregate(this.PLAYER_GAMES_AGGREGATION(id))
          ).toArray()
        )[0],
      operation: 'aggregation',
    })
  },

  /**
   * Turns an average score per round into a rating out of 5 stars
   */
  getRating(avg) {
    const bestScore = 3
    const worstScore = 20
    const maxRating = 4
    const basis =
      avg < bestScore ? bestScore : avg > worstScore ? worstScore : avg
    const rating =
      ((worstScore - basis) / (worstScore - bestScore)) * maxRating + 1
    return Math.round((rating + Number.EPSILON) * 100) / 100
  },

  async getCredit(parent, args, { user }) {
    const { STATUSES } = this.modelRegistry.games
    const gamesInProgress = (
      await this.modelRegistry.games.get({ player: user.id })
    ).filter(({ status }) =>
      [STATUSES.CREATED, STATUSES.STARTED].includes(status)
    )
    return GAMES_CREDIT_LIMIT - gamesInProgress.length
  },

  /**
   * Appends badges to players
   * @param {*} players: list of players sorted by rating desc
   * @returns players with badges
   */
  appendBadges(players) {
    const sortedCreatedAt = [...players].sort(
      (a, b) => new Date(a.createdAt) - new Date(b.createdAt)
    )
    const oldestPlayer = sortedCreatedAt?.[0]?.username
    const newestPlayer = sortedCreatedAt?.pop()?.username

    const largestNumberOfGames = Math.max(
      ...players.map(({ stats: { numberOfGames } }) => numberOfGames)
    )
    const largestNumberOfRounds = Math.max(
      ...players.map(({ stats: { numberOfRounds } }) => numberOfRounds)
    )

    const getBadges = ({
      player: {
        username,
        stats: { numberOfGames, numberOfRounds },
      },
      index,
    }) =>
      Object.entries({
        isFirst: index === 0,
        isSecond: index === 1,
        isThird: index === 2,
        oldestPlayer: oldestPlayer === username,
        newestPlayer: newestPlayer === username,
        bestRating: false,
        largestNumberOfGames: largestNumberOfGames === numberOfGames,
        largestNumberOfRounds: largestNumberOfRounds === numberOfRounds,
      }).reduce((acc, [key, value]) => (value ? acc.concat(key) : acc), [])

    return players.map((player, index) => ({
      ...player,
      stats: { ...player.stats, rank: index + 1 },
      badges: getBadges({ player, index }),
    }))
  },

  PLAYERS_STATS_AGGREGATION: (
    gamesCreatedAfter = new Date('1970').getTime(),
    gamesCreatedBefore = new Date().getTime()
  ) => [
    {
      $match: {
        deleted: false,
      },
    },
    {
      $project: {
        _id: {
          $toString: '$_id',
        },
        createdAt: 1,
        username: 1,
      },
    },
    {
      $lookup: {
        from: 'games',
        localField: '_id',
        foreignField: 'players.user',
        pipeline: [
          {
            $match: {
              status: 'finished',
              createdAt: {
                $gte: new Date(gamesCreatedAfter),
                $lt: new Date(gamesCreatedBefore),
              },
            },
          },
          {
            $project: {
              players: 1,
              numberOfRounds: 1,
              _id: 1,
            },
          },
        ],
        as: 'games',
      },
    },
    {
      $match: {
        'games.2': {
          $exists: true,
        },
      },
    },
    {
      $project: {
        username: 1,
        createdAt: 1,
        games: 1,
        scores: {
          $map: {
            input: '$games',
            as: 'r',
            in: {
              $filter: {
                input: '$$r.players',
                as: 'p',
                cond: {
                  $eq: ['$_id', '$$p.user'],
                },
              },
            },
          },
        },
      },
    },
    {
      $project: {
        id: '$_id',
        username: 1,
        createdAt: 1,
        stats: {
          numberOfGames: {
            $size: '$games',
          },
          numberOfRounds: {
            $sum: {
              $map: {
                input: '$games',
                as: 'r',
                in: '$$r.numberOfRounds',
              },
            },
          },
          averageScore: {
            $round: [
              {
                $avg: {
                  $reduce: {
                    input: '$scores',
                    initialValue: [],
                    in: {
                      $concatArrays: [
                        '$$value',
                        {
                          $arrayElemAt: ['$$this.score', 0],
                        },
                      ],
                    },
                  },
                },
              },
              2,
            ],
          },
        },
      },
    },
    {
      $sort: {
        username: 1,
      },
    },
  ],

  PLAYER_GAMES_AGGREGATION: (playerId) => [
    {
      $match: {
        _id: new ObjectId(playerId),
        deleted: false,
      },
    },
    {
      $project: {
        id: {
          $toString: '$_id',
        },
      },
    },
    {
      $lookup: {
        from: 'games',
        localField: 'id',
        foreignField: 'players.user',
        pipeline: [
          {
            $project: {
              id: {
                $toString: '$_id',
              },
              numberOfRounds: 1,
              players: 1,
              status: 1,
              createdAt: 1,
              _id: 1,
            },
          },
          {
            $sort: {
              createdAt: -1,
            },
          },
        ],
        as: 'games',
      },
    },
    {
      $project: {
        games: 1,
        otherPlayers: {
          $reduce: {
            input: '$games',
            initialValue: [],
            in: {
              $setUnion: {
                $concatArrays: [
                  '$$value',
                  {
                    $map: {
                      input: '$$this.players',
                      as: 'players',
                      in: {
                        $toObjectId: '$$players.user',
                      },
                    },
                  },
                ],
              },
            },
          },
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'otherPlayers',
        foreignField: '_id',
        as: 'otherPlayers',
        pipeline: [
          {
            $project: {
              id: {
                $toString: '$_id',
              },
              username: 1,
            },
          },
        ],
      },
    },
  ],
}

/**
 * Testing timing in the brwoser
 */

// (async () => {
//   // override fetch here
//   const newRequest = async () => fetch("https://www.lets-play-golf.com/graphql", {
//         "credentials": "include",
//         "headers": {
//             "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:109.0) Gecko/20100101 Firefox/114.0",
//             "Accept": "*/*",
//             "Accept-Language": "en-US,en;q=0.5",
//             "content-type": "application/json",
//             "Alt-Used": "www.lets-play-golf.com",
//             "Sec-Fetch-Dest": "empty",
//             "Sec-Fetch-Mode": "cors",
//             "Sec-Fetch-Site": "same-origin"
//         },
//         "referrer": "https://www.lets-play-golf.com/",
//         "body": "{\"operationName\":\"games\",\"variables\":{\"player\":\"5faec75635d98991fa66353e\"},\"query\":\"query games {\\n  games {\\n    games {\\n      id\\n      numberOfRounds\\n      players {\\n        user\\n        score\\n        __typename\\n      }\\n      status\\n      createdAt\\n      __typename\\n    }\\n    otherPlayers {\\n      id\\n      username\\n      activeGameSubscriptions\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}",
//         "method": "POST",
//         "mode": "cors"
//   });

// 	const oldRequest = async () => fetch("https://www.lets-play-golf.com/graphql", {
//     "credentials": "include",
//     "headers": {
//         "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:109.0) Gecko/20100101 Firefox/114.0",
//         "Accept": "*/*",
//         "Accept-Language": "en-US,en;q=0.5",
//         "content-type": "application/json",
//         "Alt-Used": "www.lets-play-golf.com",
//         "Sec-Fetch-Dest": "empty",
//         "Sec-Fetch-Mode": "cors",
//         "Sec-Fetch-Site": "same-origin"
//     },
//     "referrer": "https://www.lets-play-golf.com/",
//     "body": "{\"operationName\":\"games\",\"variables\":{\"player\":\"5faec75635d98991fa66353e\"},\"query\":\"query games {\\n  games {\\n    games {\\n      id\\n      numberOfRounds\\n      players {\\n        user(skipRead: true) {\\n          id\\n          __typename\\n        }\\n        score\\n        isOnline\\n        __typename\\n      }\\n      status\\n      createdAt\\n      __typename\\n    }\\n    otherPlayers {\\n      id\\n      username\\n      __typename\\n    }\\n    __typename\\n  }\\n  me {\\n    credit\\n    __typename\\n  }\\n}\\n\"}",
//     "method": "POST",
//     "mode": "cors"
// });

//   const timeRequest = async (requestFn) => {
//     const start = new Date()
//     await requestFn()
//     return new Date() - start
//   }

// 	const averageTime = async (executionCount, requestFn) => {
// 		let timings = [];
// 		for (let x = 0; x < executionCount; x++) {
// 			timings.push(await timeRequest(requestFn))
// 			console.log(timings)
// 		}
// 		return timings.reduce((acc,itm) => acc + itm,  0)/timings.length
// 	}
//   console.log('Average time for 60 executions of oldRequest', await averageTime(60, oldRequest),'ms')
// })()

/**
 * @test performance testing newRequest Vs oldRequest
 * > newRequest with user = benoit
 * Average time for 20 executions of newRequest 1191.2 ms
 * Average time for 20 executions of newRequest 1194.9 ms
 * Average time for 20 executions of newRequest 1164.25 ms
 * Average time for 20 executions of newRequest 1040.6 ms
 * Average time for 20 executions of newRequest 1028.5 ms
 * Total average time: 1123.89 ms
 *
 * > oldRequest with user = benoit
 * Average time for 20 executions of newRequest 2065.2 ms
 * Average time for 20 executions of oldRequest 2077 ms
 * Average time for 60 executions of oldRequest 2077.5333333333333 ms
 * Total average time: 2073.2444444444445
 *
 * @result newRequest performance gain: 949.3544444444444ms
 *
 *
 * @test performance testing of newRequest with plugins disabled
 * - newRequest with user = benoit
 * Average time for 100 executions of newRequest 1033.04 ms
 *
 * @result no performance gain
 */
