module.exports = {
  name: 'passwordReset',
  deps: ['modelRegistry', 'emails'],

  /**
   * Records the user's request for password reset and sends them an email with a link if they exist.
   * @param {string} options.email: customer email associated with the account
   */
  async requestPasswordReset({ email }) {
    const [user] = await this.modelRegistry.users.get({ email })
    if (!user) return true
    const resetHash = `${user.id}-${this.generateHash(24)}-${this.generateHash(
      24
    )}-${this.generateHash(24)}`
    // Send password reset email
    const { firstname } = user
    await this.emails.send({
      to: user.email,
      ...this.config.passwordResetRequestedEmail,
      variables: { firstname, resetLink: `rpr/${resetHash}` },
    })
    await this.modelRegistry.userActivity.create({
      type: 'PasswordResetRequested',
      user,
      resetHash,
    })
    return true
  },

  /**
   * Resets user's password if the resetHash matches
   * @param {string} options.resetHash: Reset hash from the link sent to the user
   * @param {string} options.password: User's new password
   */
  async resetPassword({ resetHash, password }) {
    const [user] = resetHash.split('-')
    const [latestPasswordRequest] = await this.modelRegistry.userActivity.get({
      type: this.modelRegistry.userActivity.types.PASSWORD_RESET_REQUESTED,
      createdWithin:
        this.modelRegistry.userActivity.config.passwordResetRequested.timespan,
      limit: 1,
      user,
    })
    const match =
      latestPasswordRequest && latestPasswordRequest.resetHash === resetHash
    if (!match) return false
    const updatedUser = await this.modelRegistry.users.update({
      update: { password },
      user: { id: user },
    })
    if (!updatedUser) return false
    await this.modelRegistry.userActivity.create({
      type: this.modelRegistry.userActivity.types.PASSWORD_RESET_SUCCESSFUL,
      user: user,
    })
    return true
  },

  generateHash: (length) =>
    new Array(Math.ceil(length / 9))
      .fill(0)
      .reduce((acc) => `${acc}${Math.random().toString(36).substring(2)}`, '')
      .substring(0, length),
}
