const bcrypt = require('bcryptjs')
const _ = require('lodash')

module.exports = {
  name: 'usersModel',
  deps: ['resourceModel', 'modelRegistry', 'emails'],
  collection: 'users',

  init() {
    this.modelRegistry.register('users', this)
    this.defaultPreferences = {
      emailWhenInvitationAccepted: false,
      emailWhenMessageReceivedOffline: false,
    }
  },

  async create(options) {
    const {
      firstname,
      password,
      emailWhenInvitationAccepted,
      emailWhenMessageReceivedOffline,
      res,
    } = options
    const { email, username } = this.formatEmailAndUsername(options)

    const userInputError = await this.validateUserDetails({
      email,
      username,
      password,
    })
    if (userInputError) return userInputError

    const user = await this.resourceModel.create({
      collection: this.collection,
      user: null,
      obj: {
        email,
        preferences: {
          emailWhenInvitationAccepted,
          emailWhenMessageReceivedOffline,
        },
        username,
        firstname,
        hash: await bcrypt.hashSync(password, bcrypt.genSaltSync(10)),
        createdAt: new Date(),
      },
    })

    // Send user creation email
    const { createUserEmail } = this.config
    this.emails.send({
      to: email,
      ...createUserEmail,
      variables: { firstname, username },
    })
    this.modelRegistry.userActivity.create({ type: 'UserCreated', user })

    return this.modelRegistry.sessions.create({ email, password, res })
  },

  async get(options) {
    const { id, ids } = options
    const { email, username } = this.formatEmailAndUsername(options)
    if (!(id || ids || email || username)) return
    return this.resourceModel.get({
      collection: this.collection,
      query: {
        id,
        ids,
        email,
        username,
      },
    })
  },

  async update(options) {
    const {
      user: { id },
      res,
      update: {
        password,
        firstname,
        emailWhenInvitationAccepted,
        emailWhenMessageReceivedOffline,
        resetHash,
      },
    } = options
    const { email, username } = this.formatEmailAndUsername(options.update)
    const userInputError = await this.validateUserDetails({
      email,
      username,
      password,
    })
    if (userInputError) return userInputError

    const updatedUser = await this.resourceModel.update({
      collection: this.collection,
      id,
      update: {
        email,
        firstname,
        username,
        resetHash,
        hash: password
          ? await bcrypt.hashSync(password, bcrypt.genSaltSync(10))
          : undefined,
        'preferences.emailWhenInvitationAccepted': emailWhenInvitationAccepted,
        'preferences.emailWhenMessageReceivedOffline':
          emailWhenMessageReceivedOffline,
      },
    })
    const updatedDetails = Object.keys(
      _.omitBy(
        {
          email,
          username,
          password,
          firstname,
          emailWhenInvitationAccepted,
          emailWhenMessageReceivedOffline,
          resetHash,
        },
        _.isUndefined
      )
    ).join(', ')
    this.emails.send({
      to: email,
      ...this.config.updateUserEmail,
      variables: { firstname: updatedUser.firstname, updatedDetails },
    })
    // resetPassword update does not include res
    if (res) this.modelRegistry.sessions.setCookie(updatedUser, res)
    return updatedUser
  },

  async delete({ user: { id } }) {
    const deletedObject = await this.resourceModel.delete({
      collection: this.collection,
      id,
    })
    return deletedObject.deleted
  },

  /**
   * @returns UserInputError for 'email' or 'username' if either is already used and cannot be taken
   */
  async validateUserDetails({ email, username, password }) {
    const { usernameRegex, passwordRegex, emailRegex } = this.config
    if (username && !usernameRegex.test(username))
      return this.utilities.UserInputError(
        'Username must contains at least 6 letters, numbers or dash sign',
        'INVALID_USERNAME',
        ['username']
      )
    if (password && !passwordRegex.test(password))
      return this.utilities.UserInputError(
        'Password must contain at least 6 alphanumercial characters and include a special character',
        'WEAK_PASSWORD',
        ['password']
      )
    if (email && !emailRegex.test(email))
      return this.utilities.UserInputError(
        `${email} is not a valid email address`,
        'INVALID_EMAIL',
        ['email']
      )
    if (!email && !username) return
    const [user] = await this.resourceModel.get({
      collection: this.collection,
      query: {
        $or: [{ email }, { username }],
      },
    })
    if (!user) return
    return user.email === email
      ? this.utilities.UserInputError(
          `User ${email} already exists`,
          'USER_EMAIL_ALREADY_EXISTS',
          ['email']
        )
      : this.utilities.UserInputError(
          `User ${username} already exists`,
          'USERNAME_ALREADY_EXISTS',
          ['username']
        )
  },

  formatEmailAndUsername({ email, username }) {
    if (email) email = email.trim().toLowerCase()
    if (username) username = username.trim().toLowerCase()
    return { email, username }
  },
}
