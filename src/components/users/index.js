module.exports = {
  name: 'users',
  deps: ['graphql', 'usersModel', 'playersModel', 'passwordReset', 'magicLink'],
  init() {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('users.graphql'),
      resolvers: {
        Query: {
          me: this.me,
        },
        Mutation: {
          createUser: this.createUser,
          updateUser: this.updateUser,
          deleteUser: this.deleteUser,
          requestPasswordReset: this.requestPasswordReset,
          resetPassword: this.resetPassword,
          requestMagicLink: this.requestMagicLink,
          checkMagicLink: this.checkMagicLink,
          consumeMagicLink: this.consumeMagicLink,
        },
        User: {
          credit: this.playersModel.getCredit,
          preferences: (itm) =>
            itm.preferences || this.usersModel.defaultPreferences,
        },
        UserResult: {
          __resolveType: (obj) => (obj.error ? 'UserInputError' : 'Session'),
        },
        UserUpdateResult: {
          __resolveType: (obj) => (obj.error ? 'UserInputError' : 'User'),
        },
      },
    })
  },

  async createUser(parent, args, { res }) {
    return this.usersModel.create({ ...args, res })
  },

  async updateUser(parent, args, { user, res }) {
    return this.usersModel.update({ ...args, user, res })
  },

  async deleteUser(parent, args, { user, res }) {
    return this.usersModel.delete({ ...args, user, res })
  },

  async requestMagicLink(parent, { email }) {
    return this.magicLink.requestMagicLink(email)
  },

  async checkMagicLink(parent, { hash }) {
    return this.magicLink.checkMagicLink(hash)
  },

  async consumeMagicLink(parent, { hash }, { res }) {
    return this.magicLink.consumeMagicLink({ hash, res })
  },

  async requestPasswordReset(parent, { email }) {
    return this.passwordReset.requestPasswordReset({ email })
  },

  async resetPassword(parent, { password, resetHash }) {
    return this.passwordReset.resetPassword({ password, resetHash })
  },

  async me(parent, args, { user }) {
    return (await this.usersModel.get(user))[0]
  },
}
