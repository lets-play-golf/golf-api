module.exports = {
  name: 'magicLink',
  deps: ['modelRegistry', 'passwordReset', 'emails'],

  expiryInMins: 15,

  /**
   * Creates a magic link and sends it to a registered user
   */
  async requestMagicLink(email) {
    const [user] = await this.modelRegistry.users.get({ email })
    if (!user) return true

    const hash = this.passwordReset.generateHash(124)
    await this.modelRegistry.userActivity.create({
      type: 'MagicLinkRequest',
      user: user.id,
      hash,
    })
    this.sendMagicLinkEmail({
      email: user.email,
      firstname: user.firstname,
      hash,
    })
    return true
  },

  /**
   * Returns the time left to use the magic link or 0 if not found
   */
  async checkMagicLink(hash) {
    const magicLink = await this.get(hash)
    if (!magicLink) return 0
    return (
      this.expiryInMins -
      Math.floor((new Date() - magicLink.createdAt) / 60 / 1000)
    )
  },

  /**
   * Logs in the user with a magic
   * @param {string} hash
   * @param {Response} res
   */
  async consumeMagicLink({ hash, res }) {
    const magicLink = await this.get(hash)

    if (!magicLink) return this.expiredMagicLinkResponse()

    const [user] = await this.modelRegistry.users.get({
      id: magicLink.createdBy,
    })

    if (!user) return this.expiredMagicLinkResponse()
    this.modelRegistry.sessions.setCookie(user, res)

    await this.modelRegistry.userActivity.create({
      type: 'consumeMagicLink',
      user: user.id,
      hash,
    })

    return { user }
  },

  expiredMagicLinkResponse() {
    return this.modelRegistry.users.utilities.UserInputError(
      'Magic link is expired',
      'EXPIRED_MAGIC_LINK'
    )
  },

  /**
   * Returns the magic link if it exists and has not been used
   * @param {string} hash
   */
  async get(hash) {
    const magicLinkRecords = await this.modelRegistry.userActivity.get({
      types: ['MagicLinkRequest', 'consumeMagicLink'],
      hash,
      createdWithin: this.expiryInMins * 60 * 1000,
    })
    const magicLink = magicLinkRecords.find(
      ({ type }) => type === 'MagicLinkRequest'
    )
    const isLinkUsed = magicLinkRecords.some(
      ({ type }) => type === 'consumeMagicLink'
    )
    if (magicLink && !isLinkUsed) return magicLink
  },

  /**
   * Sends the magic link by email to the user
   * @param {*} hash: magic link hash
   * @param {*} email: user's email
   * @param {*} firstname: user firstname
   */
  sendMagicLinkEmail({ hash, email, firstname }) {
    this.emails.send({
      template: 'MagicLinkRequested',
      subject: 'Login to your account',
      to: email,
      variables: {
        hash,
        linkLifeSpan: `${this.expiryInMins} minutes`,
        firstname,
      },
    })
  },
}
