const { ApolloServer, PubSub } = require('apollo-server-express')
const GraphQLJSON = require('graphql-type-json')
const { GraphQLScalarType } = require('graphql')
const _ = require('lodash')
const pubsub = new PubSub()

module.exports = {
  name: 'graphql',
  deps: ['sessionsModel', 'authorisation', 'modelRegistry', 'metrics'],
  pubsub,

  init() {
    this.setupApolloStudioReporting()
    this.schemas = []
  },

  setupApolloStudioReporting() {
    // Apollo Studio Reporting
    const { APOLLO_KEY, APOLLO_GRAPH_VARIANT, APOLLO_SCHEMA_REPORTING } =
      this.config
    process.env.APOLLO_KEY = APOLLO_KEY
    process.env.APOLLO_GRAPH_VARIANT = APOLLO_GRAPH_VARIANT
    process.env.APOLLO_SCHEMA_REPORTING = APOLLO_SCHEMA_REPORTING
  },

  registerSchema(schema) {
    this.schemas.push(schema)
  },

  async addExpressMiddleware(app) {
    this.server = new ApolloServer({
      tracing: this.config.tracing,
      introspection: this.config.introspection,
      playground: this.config.playground,
      context: ({ req, res }) => ({
        req,
        res,
        ...this.sessionsModel.get({ req, res }),
        pubsub,
      }),
      typeDefs: [
        this.load('root.graphql'),
        this.load('errors.graphql'),
        this.load('authorisations.graphql'),
        ...this.schemas.map((schema) => schema.typeDefs),
      ],
      resolvers: [
        {
          JSON: GraphQLJSON,
          Date: new GraphQLScalarType({
            name: 'Date',
            description: 'Date',
            parseValue(value) {
              return new Date(value)
            },
            serialize(value) {
              return new Date(value).toISOString()
            },
            parseLiteral({ value }) {
              if (!/\d{4}-\d{1,2}-\d{1,2}/.test(value))
                throw Error('Graphql Date input must be in format yyyy-mm-dd')
              return new Date(value)
            },
          }),
        },
        ...this.schemas
          .filter((schema) => !!schema.resolvers)
          .map(({ resolvers }) => resolvers),
      ],
      plugins: [
        this.authorisationPlugin(),
        this.errorMonitoringPlugin(),
        //this.operationTimingPlugin(),
        this.operationProfilingPlugin(),
      ],
      schemaDirectives: Object.assign(
        {},
        ...this.schemas.map((schema) => schema.schemaDirectives)
      ),
      formatError: (error) => {
        const errorDetails = _.get(error, 'originalError.response.body')
        try {
          if (errorDetails) return JSON.parse(errorDetails)
        } catch (e) {
          // continue regardless of error
        }
        return error
      },
    })
    this.authorisation.registerAuthorisationGroups(this.server.schema)
    this.server.applyMiddleware({
      app,
      cors: false,
    })
  },

  addSubscriptionsServer(server) {
    const { SubscriptionServer } = require('subscriptions-transport-ws')
    const { execute, subscribe } = require('graphql')
    this.subscriptionServer = new SubscriptionServer(
      {
        execute,
        subscribe,
        schema: this.server.schema,
        onConnect: (connectionParams, webSocket, props) => {
          const { request } = props
          return { pubsub, ...this.sessionsModel.get({ wsReq: request }) }
        },
      },
      {
        server,
        path: '/graphql',
      }
    )
  },

  /**
   * Checks user permissions and intercepts operations the user is not allowed to run
   */
  authorisationPlugin() {
    return {
      requestDidStart: () => {
        return {
          didResolveOperation: ({ request, operation, context }) => {
            const operationName =
              request.operationName ||
              operation.selectionSet.selections[0].name.value
            this.authorisation.authorise({ context, operationName })
          },
        }
      },
    }
  },

  /**
   * Records errors in the user activity collection
   */
  errorMonitoringPlugin() {
    return {
      requestDidStart: () => {
        const startTime = new Date()
        return {
          willSendResponse: ({ request, response, context: { user } }) => {
            try {
              const errors = response.errors
              if (!errors) return
              console.log(
                'Graphql errorMonitoringPlugin - error occured: ',
                JSON.stringify(errors[0])
              )
              const operationName = request.operationName
              this.modelRegistry.userActivity.create({
                type: 'GraphqlErrors',
                errors,
                operationName,
                user,
                timeTaken: new Date() - startTime,
              })
            } catch (e) {
              console.log('Error in errorMonitoringPlugin', e)
            }
          },
        }
      },
    }
  },

  /**
   * Times all graphql queries and mutations and records timings in a histogram.
   */
  operationTimingPlugin() {
    return {
      requestDidStart: () => {
        const startTime = new Date()
        return {
          willSendResponse: ({ request, response }) => {
            try {
              const timeInMs = new Date() - startTime
              const isError = response?.errors?.length > 0
              const operationName = request.operationName

              // Skip recording the timing for null, undefined and IntrospectionQuery
              if (
                [null, undefined, 'IntrospectionQuery'].includes(operationName)
              )
                return
              this.metrics.observeGraphqlOperation({
                operation: operationName,
                timeInMs,
                isError,
              })
            } catch (e) {
              console.log('Error in errorMonitoringPlugin', e)
            }
          },
        }
      },
    }
  },

  /**
   *  didResolveSource
      parsingDidStart
      validationDidStart
      didResolveOperation
      responseForOperation
      executionDidStart
      didEncounterErrors
      willSendResponse
   * @returns 
   */
  operationProfilingPlugin: () => ({
    requestDidStart: ({ request }) => {
      const startTime = new Date()
      const { operationName } = request
      if ([null, undefined, 'IntrospectionQuery'].includes(operationName))
        return

      const logTiming = (event) =>
        console.log(
          'operationProfilingPlugin',
          operationName,
          event,
          new Date() - startTime,
          'ms'
        )
      return {
        // didResolveSource: () => {
        //   logTiming('didResolveSource')
        //   return {
        //     didResolveOperation: () => {
        //       logTiming('didResolveOperation')
        //       return {
        //         responseForOperation: () => {
        //           logTiming('responseForOperation')
        //           return {
        //             executionDidStart: () => {
        //               logTiming('executionDidStart')
        //               return {
        //                 willSendResponse: () => logTiming('willSendResponse'),
        //               }
        //             },
        //           }
        //         },
        //       }
        //     },
        //   }
        // },
        parsingDidStart: () => logTiming('parsingDidStart'),
        didResolveSource: () => logTiming('didResolveSource'),
        didResolveOperation: () => logTiming('didResolveOperation'),
        responseForOperation: () => logTiming('responseForOperation'),
        executionDidStart: () => logTiming('executionDidStart'),
        willSendResponse: () => logTiming('willSendResponse'),
        willResolveField: () => logTiming('willResolveField'),
        // didResolveOperation: () => logTiming('didResolveOperation'),
        // didResolveOperation: () => logTiming('didResolveOperation'),
        // didResolveOperation: () => logTiming('didResolveOperation'),
      }
    },
  }),

  /**
   * @param {array | string} path: paths to the graphql model files to be loaded
   */
  load(path) {
    const paths = Array.isArray(path) ? path : [path]
    return paths
      .map((p) =>
        require('fs')
          .readFileSync(`${__dirname}/../schemas/${p}`)
          .toString('utf-8')
      )
      .join(' ')
  },
}
