module.exports = {
  name: 'modelRegistry',

  /**
   * Provides a single access point to all models regardless of dependency order - except in init
   */

  register(name, model) {
    this[name] = model
    return this.addUtilities(model)
  },

  addUtilities(model) {
    model.utilities = {
      UserInputError: (message, key = '', fields, data) => ({
        error: { message, key, fields, data },
      }),
    }
  },
}
