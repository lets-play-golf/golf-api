const MAX_NUMBER_OF_PLAYERS = 6

/**
 * Interface with mongo collection `games` where the state of a game is kept
 */
module.exports = {
  name: 'gamesModel',
  collection: 'games',
  deps: [
    'resourceModel',
    'modelRegistry',
    'emails',
    'gamesSubscriptions',
    'golf',
    'virtualPlayersModel',
  ],

  init() {
    this.modelRegistry.register('games', this)
  },

  STATUSES: {
    CREATED: 'created',
    STARTED: 'started',
    FINISHED: 'finished',
  },

  async create({ numberOfRounds, user }) {
    const [player] = await this.modelRegistry.users.get({ id: user.id })
    if (!player)
      throw new Error(`Could not find create a game player with id ${user.id}`)
    const players = [{ user: user.id, score: [] }]
    const chat = await this.modelRegistry.chats.create({
      isPublic: false,
      users: [user.id],
      user,
    })
    return this.resourceModel.create({
      collection: this.collection,
      user,
      obj: {
        chat: chat.id,
        players,
        status: this.STATUSES.CREATED,
        numberOfRounds,
        events: [],
        deck: this.golf.newShuffledDeck(),
      },
      formatter: this.formatter,
    })
  },

  async get({ id, status, player, chat }) {
    const query = {
      id,
      status,
      chat,
    }
    if (player) {
      query.players = {
        $elemMatch: {
          user: player,
        },
      }
    }
    return this.resourceModel.get({
      collection: this.collection,
      query,
      formatter: this.formatter,
    })
  },

  /**
   * Updates game collection and handles gameUpdates subscriptions messages
   */
  async update({
    id,
    update: { events, deck, status, players, numberOfRounds },
  }) {
    const updatedGame = await this.resourceModel.update({
      collection: this.collection,
      id,
      update: {
        events,
        deck,
        status,
        players,
        numberOfRounds,
      },
      formatter: this.formatter,
    })
    this.gamesSubscriptions.publishGameUpdated({ game: updatedGame })
    return updatedGame
  },

  // TODO: remove once documents are formatted with numberOfRounds instead of holes
  formatter(game) {
    return {
      ...game,
      numberOfRounds: game.holes || game.numberOfRounds,
    }
  },

  /**
   * Throws an error if the user is not registered against the game.
   * @param {object} game: game object
   * @param {object} user: currently signed in user
   */
  authorise({ user: { id }, game }) {
    const [match] = game.players.filter(({ user }) => user === id)
    if (!match) throw new Error(`User ${id} is not allowed to access this game`)
    return match
  },

  async join({ id, user }) {
    const [game] = await this.get({ id })
    if (!game) throw Error(`Game ${id} not found`)
    const { status, players } = game
    if (status !== this.STATUSES.CREATED)
      throw Error(`Game ${id} cannot be joined`)
    if (players.some((itm) => itm.user === user.id))
      throw Error(`User already joined game ${id}`)

    if (players.length + 1 >= MAX_NUMBER_OF_PLAYERS)
      throw Error(`Maximum number of players reached`)
    const updatedGame = await this.update({
      id,
      update: {
        players: [
          ...players,
          {
            user: user.id,
            score: [],
          },
        ],
      },
    })
    await this.modelRegistry.chats.addParticipant({
      id: updatedGame.chat,
      user,
    })
    const otherPlayers = await this.modelRegistry.users.get({
      ids: players.map(({ user }) => user),
    })

    otherPlayers.forEach(
      ({ preferences: { emailWhenInvitationAccepted }, email }) => {
        emailWhenInvitationAccepted &&
          this.emails.send({
            to: email,
            template: 'UserJoinedGame',
            subject: `${user.firstname} joined your game, play golf now!`,
            variables: {
              gamesCreditLimit: this.modelRegistry.players.GAMES_CREDIT_LIMIT,
              joinee: user.firstname,
              numberOfRounds: updatedGame.numberOfRounds,
              gameId: updatedGame.id,
            },
          })
      }
    )

    return updatedGame
  },

  async addABot({ id, bot, user }) {
    const [game] = await this.get({ id })

    this.authorise({ user, game })
    const isBotAlreadyAdded = game.players.some((itm) => itm.user === bot)
    if (isBotAlreadyAdded) throw new Error('Could not add bot to this game')

    const isBotId = this.virtualPlayersModel.isVirtualPlayer(bot)
    if (!isBotId) throw new Error(`Could not find bot ${bot}`)
    const players = game.players.concat({
      user: bot,
      score: [],
    })
    return this.update({
      id,
      update: {
        players,
      },
    })
  },

  async start({ id, user }) {
    const [game] = await this.get({ id })
    this.authorise({ user, game })
    if (game.players.length < 2)
      throw new Error('Cannot start the game as there is not enough players')
    if (game.status !== this.STATUSES.CREATED)
      throw new Error('Cannot start the game')
    return this.update({
      id,
      update: {
        status: this.STATUSES.STARTED,
      },
    })
  },
}
