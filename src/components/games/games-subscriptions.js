const { UserInputError } = require('apollo-server')

module.exports = {
  name: 'gamesSubscriptions',
  deps: ['pubsub', 'modelRegistry', 'golf', 'virtualPlayersSubscriptions'],

  /**
   * @returns player's channel name
   * @param {string} id: required - game id
   * @param {object} user: required - user id
   */
  channel({ id, user }) {
    if (!(id && user))
      throw Error(`Cannot get channel without ${!id ? 'id' : 'user'}`)
    return `GAME_CHANNEL_${id}_${user}`
  },

  /**
   * Checks user is allowed to join channel and subscribes the user
   * @param {string} options.id: the game id the user is trying to subscriobe to
   * @param {string} options.user: the currently logged in user
   * @throws error if user not authorised
   */
  async subscribe({ id, user }) {
    const [game] = await this.modelRegistry.games.get({ id, user })
    if (!game) throw new UserInputError(`Game ${id} was not found`)
    this.modelRegistry.games.authorise({ user, game })
    await this.virtualPlayersSubscriptions.handleVirtualPlayersSubscriptions({
      game,
    })
    return this.pubsub.subscribe(this.channel({ id, user: user.id }))
  },

  /**
   * Publish gameUpdated - either Game or PeekCards
   * - Sends a Game update on all channels - provide `game` only
   * - Sends a PeekCards update on a user's private channel - provide `game`, `peekCards` with { spots } and `user`
   * @param {Game} options.game: required - the game object containing the game id
   * @param {PeekCards} options.peekCards: optional - the gameObject to publish - defaulted to game
   * @param {object} options.users: optional - filter users channels to target
   */
  publishGameUpdated({ game, peekCards, user } = {}) {
    if (!game) throw Error('cannot publish update without game')

    const channelsToUpdate = this.pubsub.matchingChannels(
      user ? this.channel({ id: game.id, user }) : `GAME_CHANNEL_${game.id}`
    )
    const message = { gameUpdated: peekCards || game }
    channelsToUpdate.forEach((channel) =>
      this.pubsub.publishMessage({ channel, message })
    )
  },

  /**
   * Publish picked cards - send PeekCards update to each private channel
   * @param {object} options.game: required - the game object to publish
   */
  publishPickedCards({ game }) {
    const golf = new this.golf.Golf(game)
    game.events
      .filter(({ action }) => action === 'pick_cards')
      .forEach((pickCardEvent) =>
        this.publishGameUpdated({
          game,
          peekCards: { spots: golf.getPickedCards(pickCardEvent) },
          user: pickCardEvent.player,
        })
      )
  },

  /**
   * Returns whether a player is subscribed for a game's updates
   * @param {object} options.game: the game object
   * @param {object} options.player: player containing user property with user id
   */
  isPlayerOnline({ game, player }) {
    return this.pubsub.hasSubscribers(
      this.channel({ id: game.id, user: player.user })
    )
  },

  /**
   * Returns a list of game ids a user is subscribed to
   */
  listGamesUserIsSubscribedTo(userId) {
    return this.pubsub
      .matchingChannels(`GAME_CHANNEL_[0-9a-z]+_${userId}`)
      .map((channel) => channel.split('_')[2])
  },
}
