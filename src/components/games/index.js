module.exports = {
  name: 'games',
  deps: [
    'gamesModel',
    'gamesSubscriptions',
    'playGame',
    'graphql',
    'golf',
    'modelRegistry',
  ],

  init() {
    this.graphql.registerSchema({
      typeDefs: [
        this.graphql.load('games.graphql'),
        this.graphql.load('play-game.graphql'),
        this.graphql.load('golf.graphql'),
      ].join(' '),
      resolvers: {
        Query: {
          games: this.gamesFromAggregation,
          gamesUntyped: this.gamesFromAggregation,
        },
        Mutation: {
          createGame: this.createGame,
          updateGame: this.updateGame,
          joinGame: this.joinGame,
          addABot: this.addABot,
          startGame: this.startGame,
          // play game mutations
          start: this.start,
          pick_cards: this.pick_cards,
          pickup: this.pickup,
          place: this.place,
          discard: this.discard,
          reveal: this.reveal,
        },
        OtherPlayer: {
          activeGameSubscriptions: ({ id }) =>
            this.gamesSubscriptions.listGamesUserIsSubscribedTo(id),
        },
        Game: {
          players: (game) =>
            game.players.map((player) => {
              return {
                userId: player.user,
                ...player,
                isOnline: this.gamesSubscriptions.isPlayerOnline({
                  game,
                  player,
                }),
              }
            }),
          nextTurn: (parent) => this.golf.getNextTurn(parent),
          golf: (parent) => this.golf.getGolf(parent),
        },
        Subscription: {
          gameUpdated: {
            subscribe: this.subscribe,
          },
        },
        GameMessage: {
          __resolveType: (obj) => (obj.numberOfRounds ? 'Game' : 'PeekCards'),
        },
      },
    })
  },

  async createGame(parent, args, { user }) {
    return this.gamesModel.create({ ...args, user })
  },

  async updateGame(parent, args, { user }) {
    return this.gamesModel.update({ ...args, user })
  },

  async startGame(parent, args, { user }) {
    return this.gamesModel.start({ ...args, user })
  },

  async joinGame(parent, { id }, { user }) {
    return this.gamesModel.join({ id, user })
  },

  async addABot(parent, { id, bot }, { user }) {
    return this.gamesModel.addABot({ id, bot, user })
  },

  async subscribe(parent, { id }, { user }) {
    return this.gamesSubscriptions.subscribe({ id, user })
  },

  async start(parent, { game }, { user }) {
    const action = 'start'
    return this.playGame.addEvent({ id: game, event: { action }, user })
  },

  async pick_cards(parent, { game, spots }, { user }) {
    const action = 'pick_cards'
    return this.playGame.addEvent({ id: game, event: { action, spots }, user })
  },

  async pickup(parent, { game, pile }, { user }) {
    const action = 'pickup'
    return this.playGame.addEvent({ id: game, event: { action, pile }, user })
  },

  async place(parent, { game, spot }, { user }) {
    const action = 'place'
    return this.playGame.addEvent({ id: game, event: { action, spot }, user })
  },

  async discard(parent, { game }, { user }) {
    const action = 'discard'
    return this.playGame.addEvent({ id: game, event: { action }, user })
  },

  async reveal(parent, { game, spot }, { user }) {
    const action = 'reveal'
    return this.playGame.addEvent({ id: game, event: { action, spot }, user })
  },

  async gamesFromAggregation(parent, args, { user }) {
    return this.modelRegistry.players.gamesFromAggregation({ id: user.id })
  },
}
