const { UserInputError } = require('apollo-server')

module.exports = {
  name: 'playGame',
  deps: ['gamesModel', 'modelRegistry', 'gamesSubscriptions', 'golf'],

  init() {
    this.STATUSES = this.gamesModel.STATUSES
    this.modelRegistry.register('playGame', this)
  },

  async addEvent({ id, event: ev, user }) {
    if (!id) throw new UserInputError('Cannot add event without a game id')
    const [game] = await this.gamesModel.get({ id, user })
    if (!game) throw new UserInputError(`Game ${id} was not found`)
    this.gamesModel.authorise({ user, game })
    const event = { ...ev, player: user.id }
    this.golf.NextTurn.authorise({ game, event, user })

    // Handle and save events against the game
    const gameUpdated = await (async () => {
      const events = game.events.concat(event)

      // if the game has come to an end
      if (this.isRoundComplete({ ...game, events }))
        return this.closeRound({ game, events })

      // if all other users have clicked start, clear events and start a new round
      if (ev.action === 'start' && this.shouldStartNewRound({ game, events }))
        return this.startRound(game, events)

      // otherwise just record update
      return this.gamesModel.update({ id, update: { events } })
    })()

    // If all players have picked cards, publish picked cards on private channels
    const publishPickedCards =
      event.action === 'pick_cards' &&
      this.haveAllPlayersPickedCards(gameUpdated)
    if (publishPickedCards)
      this.gamesSubscriptions.publishPickedCards({ game: gameUpdated })

    return gameUpdated
  },

  /**
   * Starts the round:
   * - only keep latest 'start' and 'pick_cards' events
   * - shuffles the deck
   */
  async startRound(game, events) {
    const indexOfEventAfterStart = events.findIndex(
      (itm) => itm.action === 'pick_cards'
    )
    if (indexOfEventAfterStart > 1)
      events = events
        .slice(indexOfEventAfterStart)
        .filter((itm) => itm.action === 'start')

    return this.gamesModel.update({
      id: game.id,
      update: {
        deck: this.golf.newShuffledDeck(),
        events,
      },
    })
  },

  /**
   * Closes round and closes game if all rounds have been played
   * @param {*} options.game: game object including outdated events property
   * @param {*} options.events: latest events list to be added to game object
   */
  async closeRound({ game, events }) {
    const scores = new this.golf.Golf({ ...game, events }).getRoundScores({
      ...game,
      events,
    })
    const players = game.players.map((player) => ({
      ...player,
      score: [...player.score, scores[player.user]],
    }))
    // Reset events
    const updatedGame = await this.gamesModel.update({
      id: game.id,
      update: {
        players,
        events,
      },
    })
    const { numberOfRounds } = updatedGame
    // If the game ended, close the game and refresh cache
    if (updatedGame.players[0].score.length >= numberOfRounds) {
      await this.closeGame(game)
      // Refresh players cache
      await this.modelRegistry.players.cache.refresh()
      updatedGame.status = this.STATUSES.FINISHED
    }
    return updatedGame
  },

  async closeGame({ id }) {
    return this.gamesModel.update({
      id,
      update: {
        status: this.STATUSES.FINISHED,
        deck: [],
        events: [],
      },
    })
  },

  /**
   * A new round should be started when all players have clicked 'start'
   */
  shouldStartNewRound({ game: { players }, events }) {
    // On the first round, look for 1 start event per player
    const isFirstRound = !events.find((itm) => itm.action === 'pickup')
    if (isFirstRound)
      return this.countEvents({ name: 'start', events }) === players.length
    // On a subsequent round, look for 2 start events per player
    return this.countEvents({ name: 'start', events }) === players.length * 2
  },

  /**
   * Round is complete when:
   * - The last event is place or reveal; and
   * - All players have picked up 4 times
   */
  isRoundComplete({ events, players }) {
    const lastEvent = events[events.length - 1]
    if (!['place', 'reveal'].includes(lastEvent?.action)) return false
    return (
      this.countEvents({
        name: 'pickup',
        events: events,
      }) ===
      players.length * 4
    )
  },

  haveAllPlayersPickedCards({ events, players }) {
    return this.countEvents({ name: 'pick_cards', events }) === players.length
  },

  countEvents: ({ name, events }) =>
    events.filter(({ action }) => action === name).length,
}
