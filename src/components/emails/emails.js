const nodemailer = require('nodemailer')
const handlebars = require('handlebars')
const { BASE_URL } = require('../../../config/config')

module.exports = {
  name: 'emails',

  GENERIC_EMAIL_VARIABLES: {
    baseUrl: BASE_URL,
  },

  init() {
    try {
      const { nodemailerTransport } = this.config
      if (!nodemailerTransport) {
        this.enabled = false
        return console.log('Emails disabled')
      }

      // Less secure app must be allowed at: https://myaccount.google.com/lesssecureapps
      this.transporter = nodemailer.createTransport(nodemailerTransport)
      this.enabled = true
    } catch (e) {
      console.log('e', e)
      this.enabled = false
    }
  },

  /**
   * Send an email from a template
   * @param {*} to: receiver's email
   * @param {*} template: name of the template in email/template folder
   * @param {*} variables: values for template variables to be replaced in the email
   * @param {*} subject: email subject, will be put in email title as well
   * @param {*} from: defaults to transporter config email
   */
  send({
    to = this?.config?.nodemailerTransport?.auth?.user,
    template = '',
    variables,
    subject = '',
    from = this?.config?.nodemailerTransport?.auth?.user,
  }) {
    if (process.env.NODE_ENV === 'test') return
    if (!this.enabled) return console.log('Emails disabled')
    if (!to) throw new Error('Email require to send an email')

    variables = { ...this.GENERIC_EMAIL_VARIABLES, ...variables }
    const body = this.template({
      path: `${__dirname}/templates/${template}.html`,
      variables,
    })
    if (!body.length) return

    const html = this.template({
      path: `${__dirname}/templates/EMAIL_HTML.html`,
      variables: { ...variables, body, subject },
    })
    try {
      this.transporter.sendMail(
        {
          from,
          to,
          bcc: from,
          subject,
          html,
          text: body.replace(/(<([^>]+)>)/gi, ''),
        },
        function (error, info) {
          if (error) return console.log(error)
          else console.log('Email sent: ' + info.response)
        }
      )
    } catch (e) {
      console.log('Email sending error:', e)
    }
  },

  template({ path, variables }) {
    try {
      const template = require('fs').readFileSync(path, 'utf8')
      const compileFn = handlebars.compile(template)
      return compileFn(variables)
    } catch (e) {
      console.log('Email templating error: ', e)
      return ''
    }
  },
}
