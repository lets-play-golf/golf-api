/**
 * @type {UserActivity}: Represents a user activity which we want to track or retrieve: e.g. `LoginFailed` activities tracked so that 3 failures result in an `AccountLocked` activity.
 */
module.exports = {
  name: 'userActivityModel',
  deps: ['resourceModel', 'modelRegistry', 'emails'],
  collection: 'user-activity',

  types: {
    ACCOUNT_LOCKED: 'AccountLocked',
    LOGIN_FAILED: 'LoginFailed',
    PASSWORD_RESET_REQUESTED: 'PasswordResetRequested',
    GRAPHQL_ERRORS: 'GraphqlErrors',
    MAGIC_LINK_REQUESTED: 'MagicLinkRequested',
    PASSWORD_RESET_SUCCESSFUL: 'PasswordResetSuccessful',
  },

  init() {
    this.notifyAdmin = this.config.notifyAdmin
    this.modelRegistry.register('userActivity', this)
  },

  /**
   * Creates a user activity record in mongo and notifies admin if applicable
   * @param options.type: required - user activity unique type key
   * @param options.user: required - user details for email notification
   * @param options.data: optional - extra info to record
   */
  async create({ type, user, ...data }) {
    try {
      if (typeof user === 'string')
        [user] = await this.modelRegistry.users.get({ id: user })
      if (user) await this.handleNotifications({ type, user, data })
      if (type === this.types.GRAPHQL_ERRORS && !user)
        user = { id: 'guest_user', email: '' }

      return this.resourceModel.create({
        collection: this.collection,
        obj: {
          type,
          email: user.email,
          env: process.env.NODE_ENV === 'production' ? 'production' : 'dev',
          ...data,
        },
        user: { id: user.id },
      })
    } catch ({ message, stack }) {
      this.notify({
        type: 'ActivityModelFailure',
        data: { message: stack },
        user,
      })
    }
  },

  async get({
    type,
    types,
    createdWithin,
    createdAfter,
    user,
    sort,
    limit = 0,
    ...attributes
  }) {
    const query = {
      type: {
        $in: types || [type],
      },
      createdBy: user,
      ...attributes,
    }
    if (createdWithin)
      query.createdAt = { $gte: new Date(new Date().getTime() - createdWithin) }
    if (createdAfter) query.createdAt = { $gt: createdAfter }
    return this.resourceModel.get({
      collection: this.collection,
      query,
      sort: sort || { _id: -1 },
      limit,
    })
  },

  async update({ id, deleted }) {
    return this.resourceModel.update({
      id,
      update: { deleted },
      collection: this.collection,
    })
  },

  handleNotifications({ type, user, data }) {
    if (this.notifyAdmin.includes(type))
      return this.notify({ type, user, data })
  },

  /**
   * Sends a user activity monitoring email to the admin.
   * @param {string} options.type: required - Activity type from the list `this.types`
   * @param {object} options.user: required - User id or user object
   * @param {object} options.data: optional - @todo: add data properties
   */
  async notify({ type, user, data = {} }) {
    const { username, email, firstname } = user
    this.emails.send({
      template: 'ActivityNotification',
      subject: `Activity monitoring: ${type}`,
      variables: {
        type,
        username,
        email,
        firstname,
        data: Object.entries(data)
          .map(([key, value]) => `${key}: ${value}`)
          .join(';  '),
      },
    })
  },

  /**
   *
   * @return {Date} result.accountLock: date when the account will be unlocked or undefined if account is not locked.
   * @return {Array} result.failedAttempts: failed attempts user-activities
   */
  async getAccountLock({ user }) {
    const { allowedFailedAttempts, timespan } = this.config.accountLock
    const userActivity = await this.get({
      types: [this.types.LOGIN_FAILED, this.types.PASSWORD_RESET_SUCCESSFUL],
      user: user.id,
      createdWithin: timespan,
    })
    const failedAttempts = userActivity
      .reverse()
      .reduce((acc, { type, createdAt }) => {
        // PASSWORD_RESET_SUCCESSFUL resets the number of attempts to 0
        if (type === this.types.PASSWORD_RESET_SUCCESSFUL) return []
        if (type === this.types.LOGIN_FAILED) return acc.concat(createdAt)
        return acc
      }, [])
    if (failedAttempts.length < allowedFailedAttempts)
      return { accountLock: false, failedAttempts }
    return {
      accountLock: new Date(
        failedAttempts.sort((a, b) => a - b)[0].getTime() + timespan
      ),
      failedAttempts,
    }
  },
}
