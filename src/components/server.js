const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const app = express()
const cookieParser = require('cookie-parser')
const path = require('path')
const promBundle = require('express-prom-bundle')
const fs = require('fs')
const PATH_TO_PUBLIC_FILES = path.join(__dirname, '../../public-files')

module.exports = {
  name: 'server',
  deps: ['graphql'],

  async init() {
    try {
      // Express App middlewares
      app.use(morgan('dev'))
      app.use(require('body-parser').json())
      app.use(cors(this.config.graphql.cors))
      app.use(cookieParser())

      this.setupMetricsEndpoint()

      const servesPublicFiles = fs.existsSync(PATH_TO_PUBLIC_FILES)
      if (!servesPublicFiles)
        this.debugPublicFilesEndpoint({ servesPublicFiles })
      if (servesPublicFiles) this.setupPublicFilesEndpoint()
    } catch (err) {
      console.log('Error occured while starting the server:', err)
    }
  },

  /**
   * Starts up server - call when all components are initialised
   */
  async startServer() {
    const { createServer } = require('http')
    const { port, host, protocol, endpoint } = this.config.graphql

    const server = await new Promise((resolve) => {
      // Start server
      this.graphql.addExpressMiddleware(app)
      const server = createServer(app).listen(port, () => {
        console.log(
          `Api available on : ${protocol}://${host}:${port}/${endpoint}`,
          '-',
          'Server listening on port',
          port,
          'PID',
          process.pid
        )
        resolve(server)
      })
    })
    this.graphql.addSubscriptionsServer(server)
  },

  /**
   * Sets up static file middleware and route for production server to serve client files
   */
  setupPublicFilesEndpoint() {
    app.use(express.static(PATH_TO_PUBLIC_FILES))

    // Open route to pages handled by react
    app.get('/*', (_, res) =>
      res.sendFile(`${PATH_TO_PUBLIC_FILES}/index.html`)
    )
    app.get('/graphql', (_, res, next) => next())
  },

  /**
   * Sets up the prom client and the metrics endpoint on `/metrics`
   * @see https://www.npmjs.com/package/express-prom-bundle
   */
  setupMetricsEndpoint() {
    const metricsMiddleware = promBundle({
      includeMethod: true,
    })

    app.use(metricsMiddleware)
  },
  debugPublicFilesEndpoint({ servesPublicFiles }) {
    console.log({ servesPublicFiles })
    console.log({
      topLevel: fs.readdirSync(path.join(__dirname, '../..')),
    })
    console.log({
      publicFiles: fs.readdirSync(path.join(__dirname, '../../public-files')),
    })
  },
}
