module.exports = {
  name: 'pubsub',
  deps: ['graphql'],

  /**
   * Provides a single access to the pubsub object
   */
  init() {
    this.pubsub = this.graphql.pubsub
  },

  /**
   * Returns asyncIterator for the specified channel
   * @param {string} channel: the channel to subscribe to
   */
  subscribe(channel) {
    if (!channel) throw Error('Cannot subscribe without a channel')
    return this.pubsub.asyncIterator(channel)
  },

  /**
   * Publishes a message on a channel
   * @param {object} options
   * @param {string} options.channel: the channel to publish to
   * @param {object} options.message: the message as defined in the graphql schema
   */
  publishMessage({ channel, message }) {
    if (!channel || !message)
      throw Error(
        `Cannot publish message without ${!channel ? 'channel' : 'message'}.`
      )
    // Use JSON to create copy of message object as pubsub.publish mutates the object and changes array order
    message = JSON.parse(JSON.stringify(message))
    return this.pubsub.publish(channel, message)
  },

  /**
   * Returns wether there are active subscribers to the channel
   * @param {string} channelName: the channel to check
   */
  hasSubscribers(channel) {
    return !!Object.values(this.pubsub.subscriptions).some(
      (subscription) => subscription[0] === channel
    )
  },

  /**
   * Returns channels matching the pattern
   * @param {string | Regex} pattern: regex pattern
   */
  matchingChannels(pattern) {
    const channels = Object.values(this.pubsub.subscriptions).reduce(
      (acc, [channel]) =>
        new RegExp(pattern).test(channel) ? [...acc, channel] : acc,
      new Set()
    )
    return [...channels]
  },
}
