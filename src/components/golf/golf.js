const { UserInputError } = require('apollo-server')

/**
 * Golf - Main class running as a singleton representing the golf game
 * - Has players and card decks
 * - Acts as an central event processor to recreate the game state
 * - Provides a publicView of the game so all players can see everything that is publicly visible
 * @param {Game} game: a started game with players, eventsa and a deck.
 */
class Golf {
  constructor({ players, deck, events }) {
    // Setup decks
    this.decks = new Decks([...deck])

    // Sort players before dealing as we cannot guarantee game.players order consistency
    this.players = [...players]
      .sort((a, b) => a.user.localeCompare(b.user))
      .map(({ user }) => new Player(user, this.decks))
    this.setup()
    this.processEvents([...events])
  }

  /**
   * Sets up the game:
   * - deals 4 cards face down to each players
   * - flips card over to discard pile
   */
  setup() {
    new Array(this.players.length * 4).fill().forEach((_, index) => {
      const cardIndex = this.decks.pickup()
      const playerIndex = index % this.players.length
      this.players[playerIndex].deal(cardIndex)
    })
    this.decks.discard(this.decks.pickup())
  }

  /**
   * Event processor to restore the game's state at each step:
   * - Finds the player at the origin of the event
   * - Executes the event function from the player class
   */
  processEvents(events) {
    events
      .filter(({ action }) =>
        ['pickup', 'discard', 'reveal', 'place'].includes(action)
      )
      .forEach(({ player: userId, action, ...otherProps }) => {
        // Find player
        const player = this.players.find(({ id }) => id === userId)

        // Execute player action
        if (action === 'pickup') return player.pickup(otherProps.pile)
        if (action === 'discard') return player.discard()
        if (action === 'reveal') return player.reveal(otherProps.spot)
        if (action === 'place') return player.place(otherProps.spot)
        throw new UserInputError('event not found')
      })
  }

  /**
   * Exposes the game's public view - everything that is publicly visible to display the game in the UI.
   * - players' cards and pickedUpCard
   * - discard pile card in the deck
   */
  getPublicView() {
    return {
      players: this.players.map((itm) => itm.publicView()),
      discardPileCard: this.decks.discardPile.topCard(),
    }
  }

  /**
   * Returns the user's hand with the two cards they picked as visible
   * @param {PickCardsEvent} event: event with action 'pick_cards', a player and the selected spots
   */
  getPickedCards({ player, spots }) {
    return this.players.find((itm) => itm.id === player).peekCards(spots)
  }

  /**
   * Once the round is completed, gets each players score
   * @returns {array} of scores
   */
  getRoundScores() {
    return this.players.reduce(
      (acc, player) => ({
        ...acc,
        [player.id]: player.countScore(this.scoreByRank),
      }),
      {}
    )
  }

  /**
   * Returns the score of a single card by rank
   * @param {string} rank
   */
  scoreByRank(rank) {
    return +{
      [rank]: rank,
      J: 10,
      Q: 10,
      K: 0,
    }[rank]
  }
}

/**
 * Player:
 * - Represents individual players state, including their cards, pickedUpCard and id.
 * - The actions available to them are directly mapped to the event processor - pickup, discard, reveal, place
 */
class Player {
  constructor(id, decks) {
    this.id = id
    this.spots = []
    this.pickedUpCard = null
    this.decks = decks
  }

  /**
   * Pickup event - can only be requested at the begining of the user's turn
   * - picks up a card from the pickup pile (unknown card) or the discard pile (face up card)
   * @param {String} pile: 'PICKUP' or 'DISCARD' depending which pile to pickup from
   */
  pickup(pile) {
    this.pickedUpCard =
      pile === 'DISCARD'
        ? this.decks.discardPile.take()
        : this.decks.pickupPile.take()
  }

  /**
   * Discard event - after `pickup` event only
   * - discards the player's picked up card
   */
  discard() {
    this.decks.discard(this.pickedUpCard)
    this.pickedUpCard = null
  }

  /**
   * Reveal event - after `discard` event only
   * @param {number} spotIndex: spot index where the card is revealed
   */
  reveal(spotIndex) {
    this.spots[spotIndex].isVisible = true
  }

  /**
   * Place event - after `pickup` event only
   * @param {number} spotIndex: spot index where the card is placed
   */
  place(spotIndex) {
    this.decks.discard(this.spots[spotIndex].cardIndex)
    this.spots[spotIndex].cardIndex = this.pickedUpCard
    this.spots[spotIndex].isVisible = true
    this.pickedUpCard = null
  }

  // Receive cards being dealt at setup stage
  deal(cardIndex) {
    this.spots.push(new Spot({ cardIndex, isVisible: false }))
  }

  peekCards(spots) {
    return new Array(4)
      .fill()
      .map((_, index) =>
        spots.includes(index) ? this.spots[index].cardIndex : null
      )
  }

  // Exposes the player's visible cards to the
  publicView() {
    return {
      id: this.id,
      cards: this.spots.map((itm) => (itm.isVisible ? itm.cardIndex : null)),
      pickedUpCard: this.pickedUpCard,
    }
  }

  /**
   * Counts the players' score for this round
   * @returns {number} score for this round
   */
  countScore(scoreByRank) {
    const countByRank = this.spots
      .map(({ cardIndex }) => getCard(cardIndex).rank)
      .reduce((acc, itm) => ({ ...acc, [itm]: acc[itm] + 1 || 1 }), {})

    return Object.entries(countByRank)
      .map(([rank, count]) => {
        if (count === 4) return -20
        if (count === 2) return 0
        return scoreByRank(rank)
      })
      .reduce((acc, itm) => acc + itm, 0)
  }
}

/**
 * Represents the pickup and discard piles in the center of the game
 */
class Decks {
  constructor(deck) {
    this.pickupPile = new Pile(deck)
    this.discardPile = new Pile()
  }
  pickup() {
    return this.pickupPile.take()
  }
  discard(cardIndex) {
    this.discardPile.put(cardIndex)
  }
}

/**
 * Encapsulates pile logic to manage a list of cards
 */
class Pile {
  constructor(cards = []) {
    this.cards = cards
  }
  take() {
    return this.cards.shift()
  }
  put(cardIndex) {
    return this.cards.unshift(cardIndex)
  }
  topCard() {
    return this.cards[0]
  }
}

/**
 * Tracks player's spots where cards may be visible or not
 */
class Spot {
  constructor({ cardIndex, isVisible }) {
    this.cardIndex = cardIndex
    this.isVisible = isVisible
  }
}

const NextTurn = {
  /**
   * Gets who is expected to play next and what action is available to them.
   * @param {Game} game: A started game coming from the DB
   * @returns {object} NextTurn: a list of actions and players who are expected to play next
   */
  get({ events, players: gamePlayers, status }) {
    if (status === 'finished') return this.format([], [])
    const players = gamePlayers.map((itm) => itm.user)

    // [start]: Get a list of users who have not started
    const playersMissingStart = players.filter(
      (player) =>
        !events.find((itm) => itm.action === 'start' && itm.player === player)
    )
    if (playersMissingStart.length)
      return this.format(['start'], playersMissingStart)

    // [pick_cards]: Get a list of users who have not picked cards
    const playersMissingPickedCards = players.filter(
      (player) =>
        !events.find(
          (itm) => itm.action === 'pick_cards' && itm.player === player
        )
    )
    if (playersMissingPickedCards.length)
      return this.format(['pick_cards'], playersMissingPickedCards)

    const [lastEvent] = events.slice(-1)

    // First player's turn
    if (lastEvent.action === 'pick_cards') {
      // If it is the 1st round, the player who created the game starts
      if (!gamePlayers[0].score.length)
        return this.format(['pickup'], [players[0]])
      // If this is not the 1st round, the player who scored the least at the last round starts
      const sortedByLastRoundScore = gamePlayers.sort(
        (a, b) => a.score.slice(-1)[0] - b.score.slice(-1)[0]
      )
      return this.format(['pickup'], [sortedByLastRoundScore[0].user])
    }

    // If last turn is Incomplete: same player as last has to complete their turn
    if (lastEvent.action === 'pickup') {
      return lastEvent.pile === 'PICKUP'
        ? this.format(['place', 'discard'], [lastEvent.player])
        : this.format(['place'], [lastEvent.player])
    }

    if (lastEvent.action === 'discard')
      return this.format(['reveal'], [lastEvent.player])

    // If last turn is complete: either it is the next player's turn or it is the end of the round
    const isRoundComplete =
      events.filter(({ action }) => action === 'pickup').length ===
      players.length * 4

    if (!isRoundComplete) {
      const nextPlayersIndex =
        (players.findIndex((itm) => itm === lastEvent.player) + 1) %
        players.length
      return this.format(['pickup'], [players[nextPlayersIndex]])
    }

    // Ignore the `start` events for the completed round
    const indexOfEventAfterStart = events.findIndex(
      (itm) => itm.action === 'pick_cards'
    )
    const nextRoundStartEvents = events
      .slice(indexOfEventAfterStart)
      .filter((itm) => itm.action === 'start')

    // [start]: Get a list of users who have not started
    const playersMissingStartNext = players.filter(
      (player) =>
        !nextRoundStartEvents.find(
          (itm) => itm.action === 'start' && itm.player === player
        )
    )
    if (playersMissingStartNext.length)
      return this.format(['start'], playersMissingStartNext)

    return {}
  },

  /**
   * Validates an event against the next turn
   * @param {EventObject} options.event: The proposed event with player and action to be evaluated
   * @param {Game} options.game: A started game object
   * @returns {boolean} wether the event matches the next turn
   */
  isValid({ event: { player, action }, game }) {
    const nextTurn = this.get(game)
    const isValidPlayer = nextTurn.players.includes(player)
    const isValidAction = nextTurn.actions.includes(action)
    const isValidEvent = isValidPlayer && isValidAction
    return isValidEvent
  },

  /**
   * Throws errors related to NextTurn
   * @param {EventObject} options.event: The proposed event with player and action to be evaluated
   * @param {Game} options.game: A started game object
   * @param {Game} options.user: The logged in user
   */
  authorise({ event: { player, action, spot }, game, user }) {
    const nextTurn = this.get(game)
    const isValidPlayer = nextTurn.players.includes(player)
    const isValidAction = nextTurn.actions.includes(action)
    if (!isValidPlayer)
      throw new UserInputError(`It is not player ${player}'s turn to play.`)
    if (!isValidAction)
      throw new UserInputError(
        `Player ${player} must complete their turn by playing the actions '${nextTurn.actions.join(
          "'  or '"
        )}'`
      )
    if (['place', 'reveal'].includes(action)) {
      const takenSpots = game.events
        .filter(
          ({ player, action }) =>
            player === user.id && ['place', 'reveal'].includes(action)
        )
        .map(({ spot }) => spot)
      if (takenSpots.includes(spot))
        throw new UserInputError(`Spot is not available`)
    }
  },

  /**
   * Formats NextTurn object
   * @param {array} actions: available actions
   * @param {array} players: players whose turn it is
   * @returns NextTurn object
   */
  format(actions, players) {
    return { actions, players }
  },
}

const SUITS = ['♥️', '♦️', '♠️', '♣️']
const RANKS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']

const getCard = (index) => {
  if (!Number.isInteger(index)) return null
  const rank = RANKS[index % RANKS.length]
  const suit = SUITS[(index - (index % RANKS.length)) / 13]
  return {
    index,
    id: `${rank}${suit}`,
    suit,
    rank,
  }
}

const newDeck = () => new Array(52).fill(0).map((v, i) => i)
const random = () => Math.random() - 0.5
const newShuffledDeck = () => newDeck().sort(random).sort(random)

module.exports = {
  name: 'golf',
  deps: ['modelRegistry'],
  NextTurn,
  Golf,
  newShuffledDeck,

  /**
   * getGolf resolver
   * @param {Game} game: Game data object
   * @returns Golf game object
   */
  getGolf(game) {
    if (!game.deck) return null
    return {
      publicView: new Golf(game).getPublicView(),
      nextTurn: NextTurn.get(game),
    }
  },

  /**
   * getNextTurn resolver
   * @param {*} game
   * @returns
   */
  async getNextTurn(game) {
    if (game.status !== this.modelRegistry.games.STATUSES.STARTED) return null
    return NextTurn.get({
      events: game.events,
      players: game.players,
    })
  },
}
