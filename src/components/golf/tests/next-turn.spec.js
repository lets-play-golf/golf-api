const { createComponent } = require('madhouse')
const {
  playGames: [playGame],
} = require('../../../../tests/fixtures/games')
const { NextTurn } = createComponent(require('../golf'))

/**
 * Returns the game with only the first x amount of events
 * @param {number} eventCount: number of events to be kept
 */
const gameTimeMachine = (eventCount) => ({
  ...playGame,
  events: events.slice(0, eventCount),
})

describe('NextTurn', () => {
  describe('get', () => {
    describe('next turn is start', () => {
      it('should return start when there are no events', () => {
        const playGame = gameTimeMachine(0)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['start'])
        expect(players).toEqual([
          '631ed3062d01daa09a083ce7',
          '631ed3062d01daa09a083ce9',
        ])
      })

      it('should return start when a player has not yet started', () => {
        const playGame = gameTimeMachine(1)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['start'])
        expect(players).toEqual(['631ed3062d01daa09a083ce9'])
      })
    })
    describe('next turn is pick_cards', () => {
      it('should return pick_cards when all players have started', () => {
        const playGame = gameTimeMachine(2)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pick_cards'])
        expect(players).toEqual([
          '631ed3062d01daa09a083ce7',
          '631ed3062d01daa09a083ce9',
        ])
      })
      it('should return pick_cards when not all players have picked cards', () => {
        const playGame = gameTimeMachine(3)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pick_cards'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
    })
    describe('first turn', () => {
      it('should be the player who created the game to pick up when it is the first round', () => {
        const playGame = gameTimeMachine(4)
        playGame.players = playGame.players.map((itm) => ({
          ...itm,
          score: [],
        }))
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })

      it('should be the player who scored the least amount of points at the last round to pick up', () => {
        const playGame = gameTimeMachine(4)
        playGame.players = playGame.players.map((itm, index) => ({
          ...itm,
          score: [10 - index],
        }))
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce9'])
      })
    })

    describe('after a user completes a turn', () => {
      it('should return pickup event for the other player', () => {
        const playGame = gameTimeMachine(7)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
    })

    describe('pickup from pickup pile, discard, reveal', () => {
      it('should return `discard` and `place` event after the user picks up', () => {
        const playGame = gameTimeMachine(8)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['place', 'discard'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
      it('should return `reveal` event after the user has placed a card', () => {
        const playGame = gameTimeMachine(9)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['reveal'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
      it('should return `pickup` event for other player when the turn is complete', () => {
        const playGame = gameTimeMachine(10)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce9'])
      })
    })

    describe('pickup from pickup pile and place', () => {
      it('should return `discard` and `place` event after the user picks up', () => {
        const playGame = gameTimeMachine(11)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['place', 'discard'])
        expect(players).toEqual(['631ed3062d01daa09a083ce9'])
      })
      it('should return `pickup` event for other player when the turn is complete', () => {
        const playGame = gameTimeMachine(12)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
    })

    describe('pickup from discard pile and place', () => {
      it('should return `place` event after the user picks up', () => {
        const playGame = gameTimeMachine(19)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['place'])
        expect(players).toEqual(['631ed3062d01daa09a083ce7'])
      })
      it('should return `pickup` event for other player when the turn is complete', () => {
        const playGame = gameTimeMachine(20)
        const { actions, players } = NextTurn.get(playGame)
        expect(actions).toEqual(['pickup'])
        expect(players).toEqual(['631ed3062d01daa09a083ce9'])
      })
    })

    describe('round is complete', () => {
      it('should return `start` for all players', () => {
        const playGame = gameTimeMachine(26)
        const { actions, players } = NextTurn.get(playGame)
        expect({ actions, players }).toEqual({
          actions: ['start'],
          players: ['631ed3062d01daa09a083ce7', '631ed3062d01daa09a083ce9'],
        })
      })
      it('should return `start` for those who have not started', () => {
        const playGame = gameTimeMachine(27)
        const { actions, players } = NextTurn.get(playGame)
        expect({ actions, players }).toEqual({
          actions: ['start'],
          players: ['631ed3062d01daa09a083ce7'],
        })
      })
      it('should return `start` for those who have not started', () => {
        const playGame = gameTimeMachine(28)
        expect(NextTurn.get(playGame)).toEqual({})
      })
    })

    describe('game is finished', () => {
      it('should return empty actions and players', () => {
        const playGame = { status: 'finished' }
        const { actions, players } = NextTurn.get(playGame)
        expect({ actions, players }).toEqual({
          actions: [],
          players: [],
        })
      })
    })
  })

  describe('authorise', () => {
    describe('Correct actions', () => {
      it.todo(
        'should not error when user and action are correct - pickup and place actions'
      )
      it.todo(
        'should not error when user, action and spot are correct - place and place reveal actions'
      )
    })
    describe('Incorrect actions', () => {
      it.todo('should error when the wrong user is trying to play')
      it.todo('should error when a user is attempting to play the wrong action')
      it.todo(
        'should error when a user is attempting to play a card in a spot that is unavailable'
      )
    })
  })
})

const events = [
  {
    action: 'start',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'start',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pick_cards',
    spots: [1, 2],
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pick_cards',
    spots: [1, 2],
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'reveal',
    spot: 1,
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'reveal',
    spot: 0,
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'place',
    spot: 3,
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'reveal',
    spot: 3,
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'reveal',
    spot: 2,
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pickup',
    pile: 'DISCARD',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'place',
    spot: 1,
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'reveal',
    spot: 0,
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'pickup',
    pile: 'PICKUP',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'discard',
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'reveal',
    spot: 2,
    player: '631ed3062d01daa09a083ce7',
  },
  {
    action: 'start',
    player: '631ed3062d01daa09a083ce9',
  },
  {
    action: 'start',
    player: '631ed3062d01daa09a083ce7',
  },
]
