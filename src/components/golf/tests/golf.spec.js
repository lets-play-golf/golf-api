const { createComponent } = require('madhouse')
const { Golf } = createComponent(require('../golf'))

const STEPS = {
  INITIAL_GAME_SETUP: 0,
  /**
   * Scenario 1: Pickup from pickup pile, discard and reveal
   */
  PICKUP_P_DISCARD_REVEAL: {
    PICKUP: 1,
    DISCARD: 2,
    REVEAL: 3,
  },

  /**
   * Scenario 2: Pickup from pickup pile and place
   */
  PICKUP_P_PLACE: {
    PICKUP: 7,
    PLACE: 8,
  },

  /**
   * Scenario 3: Pickup from discard pile and place
   */
  PICKUP_D_PLACE: {
    PICKUP: 14,
    PLACE: 15,
  },

  /**
   * Scenario 3: Pickup from discard pile and place
   */
  NEXT_ROUND: {
    START_1: 22,
    START_2: 23,
  },
}

describe('Golf', () => {
  describe('getPublicView', () => {
    describe('events processing', () => {
      it('should retrieve the initial state at game setup', () => {
        const { INITIAL_GAME_SETUP } = STEPS
        const playGame = gameTimeMachine(INITIAL_GAME_SETUP)
        const golf = new Golf(playGame)
        const { publicView } = expectedGameStates[INITIAL_GAME_SETUP]
        expect(golf.getPublicView()).toEqual(publicView)
      })

      describe('pickup from pickup pile, discard and reveal', () => {
        const {
          PICKUP_P_DISCARD_REVEAL: { PICKUP, DISCARD, REVEAL },
        } = STEPS

        it('should process the pickup event', () => {
          const playGame = gameTimeMachine(PICKUP)
          const golf = new Golf(playGame)
          const { publicView } = expectedGameStates[PICKUP]
          expect(publicView).toEqual(golf.getPublicView())
        })

        it('should process the discard event', () => {
          const playGame = gameTimeMachine(DISCARD)
          const golf = new Golf(playGame)
          const { publicView } = expectedGameStates[DISCARD]
          expect(publicView).toEqual(golf.getPublicView())
        })

        it('should process the reveal event', () => {
          const playGame = gameTimeMachine(REVEAL)
          const golf = new Golf(playGame)
          const { publicView } = expectedGameStates[REVEAL]
          expect(publicView).toEqual(golf.getPublicView())
        })
      })

      describe('pickup from pickup pile and place', () => {
        const {
          PICKUP_P_PLACE: { PICKUP, PLACE },
        } = STEPS
        it('should process the pickup event', () => {
          const playGame = gameTimeMachine(PICKUP)
          const golf = new Golf(playGame)
          const { publicView } = expectedGameStates[PICKUP]
          expect(publicView).toEqual(golf.getPublicView())
        })

        it('should process the place event', () => {
          const playGame = gameTimeMachine(PLACE)
          const golf = new Golf(playGame)
          const { publicView } = expectedGameStates[PLACE]
          expect(publicView).toEqual(golf.getPublicView())
        })

        describe('pickup from discard pile and place', () => {
          const {
            PICKUP_D_PLACE: { PICKUP, PLACE },
          } = STEPS

          it('should process the pickup event', () => {
            const playGame = gameTimeMachine(PICKUP)
            const golf = new Golf(playGame)
            const { publicView } = expectedGameStates[PICKUP]
            expect(publicView).toEqual(golf.getPublicView())
          })

          it('should process the place event', () => {
            const playGame = gameTimeMachine(PLACE)
            const golf = new Golf(playGame)
            const { publicView } = expectedGameStates[PLACE]
            expect(publicView).toEqual(golf.getPublicView())
          })
        })

        describe('next round', () => {
          const {
            NEXT_ROUND: { START_1, START_2 },
          } = STEPS

          it('should keep the state as it is as the players start the next round', () => {
            const playGame = gameTimeMachine(START_1)
            const golf = new Golf(playGame)
            const { publicView } = expectedGameStates[START_1]
            expect(publicView).toEqual(golf.getPublicView())
          })

          it('should process the place event', () => {
            const playGame = gameTimeMachine(START_2)
            const golf = new Golf(playGame)
            const { publicView } = expectedGameStates[START_2]
            expect(publicView).toEqual(golf.getPublicView())
          })
        })

        describe('final state', () => {
          it('should process correctly all the games events until the final state', () => {
            playGameFixture.events.slice(4, -1).forEach((_, index) => {
              const playGame = gameTimeMachine(index)
              const golf = new Golf(playGame)
              const { publicView } = expectedGameStates[index]
              expect(publicView).toEqual(golf.getPublicView())
            })
          })
        })
      })
    })
  })

  describe('getPickedCards', () => {
    it('should return the players cards in spots 1 and 2', () => {
      const pickCardsEvent = {
        action: 'pick_cards',
        spots: [1, 2],
        player: '631ed3062d01daa09a083ce9',
      }
      const playGame = gameTimeMachine(0)
      const requestedCards = new Golf(playGame).getPickedCards(pickCardsEvent)
      expect(requestedCards).toEqual([null, 17, 4, null])
    })
    it('should return the players cards in spots 0 and 3', () => {
      const pickCardsEvent = {
        action: 'pick_cards',
        spots: [0, 3],
        player: '631ed3062d01daa09a083ce7',
      }
      const playGame = gameTimeMachine(0)
      const requestedCards = new Golf(playGame).getPickedCards(pickCardsEvent)
      expect(requestedCards).toEqual([33, null, null, 12])
    })
  })

  describe('getRoundScores', () => {
    it('should calculate the roundScores correctly', () => {
      const playGame = gameTimeMachine(playGameFixture.events.length)
      const roundScores = new Golf(playGame).getRoundScores()
      expect(roundScores).toEqual({
        '631ed3062d01daa09a083ce7': 8,
        '631ed3062d01daa09a083ce9': 1,
      })
    })
  })
})

/**
 * Returns the game with only the first x amount of events
 * @param {number} eventCount: number of events to be kept
 */
const gameTimeMachine = (eventCount) => ({
  ...playGameFixture,
  events: [...playGameFixture.events.slice(0, 4 + eventCount)],
})

const playGameFixture = {
  _id: {
    $oid: '631b8d15e9e304003c04dc48',
  },
  chat: '6282331c813801dceb790d91',
  players: [
    {
      user: '631ed3062d01daa09a083ce7',
      score: [22],
    },
    {
      user: '631ed3062d01daa09a083ce9',
      score: [19],
    },
  ],
  status: 'started',
  numberOfRounds: 5,
  createdAt: {
    $date: {
      $numberLong: '1652699932989',
    },
  },
  createdBy: '631ed3062d01daa09a083ce7',
  deleted: false,
  deck: [
    33, 51, 45, 17, 35, 4, 12, 49, 26, 19, 10, 13, 34, 9, 30, 3, 20, 7, 2, 36,
    6, 1, 16, 42, 37, 27, 15, 50, 40, 41, 0, 43, 29, 48, 22, 44, 32, 18, 39, 24,
    28, 31, 38, 5, 8, 11, 23, 25, 47, 21, 14, 46,
  ],
  events: [
    {
      action: 'start',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'start',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pick_cards',
      spots: [1, 2],
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pick_cards',
      spots: [1, 2],
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'reveal',
      spot: 1,
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'reveal',
      spot: 0,
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'place',
      spot: 3,
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'reveal',
      spot: 3,
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'reveal',
      spot: 2,
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pickup',
      pile: 'DISCARD',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'place',
      spot: 1,
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'reveal',
      spot: 0,
      player: '631ed3062d01daa09a083ce9',
    },
    {
      action: 'pickup',
      pile: 'PICKUP',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'discard',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'reveal',
      spot: 2,
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'start',
      player: '631ed3062d01daa09a083ce7',
    },
    {
      action: 'start',
      player: '631ed3062d01daa09a083ce9',
    },
  ],
  updatedAt: {
    $date: {
      $numberLong: '1662966128776',
    },
  },
}

const expectedGameStates = [
  {
    publicView: {
      discardPileCard: 26,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 26,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, null, null, null],
          pickedUpCard: 19,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 19,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 19,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 19,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: 10,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 10,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [null, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 10,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, null],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 10,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, null],
          pickedUpCard: 13,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 49,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 49,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, null],
          pickedUpCard: 34,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 34,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, null],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 34,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 34,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: 9,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 9,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, null, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 9,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 34,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, null, null, 12],
          pickedUpCard: 9,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 45,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 45,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, 4, 13],
          pickedUpCard: 30,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 30,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [null, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 30,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [51, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 30,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: 3,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [51, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 3,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, null, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [51, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 3,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, 35, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [51, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
  {
    publicView: {
      discardPileCard: 3,
      players: [
        {
          id: '631ed3062d01daa09a083ce7',
          cards: [33, 9, 35, 12],
          pickedUpCard: null,
        },
        {
          id: '631ed3062d01daa09a083ce9',
          cards: [51, 17, 4, 13],
          pickedUpCard: null,
        },
      ],
    },
  },
]
