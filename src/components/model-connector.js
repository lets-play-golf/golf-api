const { SchemaDirectiveVisitor } = require('apollo-server')
const { GraphQLList } = require('graphql')
const _ = require('lodash')

module.exports = {
  name: 'modelConnector',
  deps: ['graphql', 'modelRegistry'],
  init() {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('model.graphql'),
      schemaDirectives: {
        belongsTo: this.createBelongsToDirective(),
      },
    })
  },

  createBelongsToDirective() {
    const component = this
    return class BelongsToDirective extends SchemaDirectiveVisitor {
      visitFieldDefinition(field) {
        field.resolve = async (parent, args, context) => {
          const options = { ...args }
          const { foreignKey, primaryKey, model, args: params } = this.args
          if (foreignKey) {
            if (!_.get(parent, foreignKey)) return null
            const param = _.get(parent, foreignKey)
            // Swap id for ids when an array of ids is passed
            const key =
              primaryKey === 'id' && Array.isArray(param) ? 'ids' : primaryKey
            options[key] = param
          }

          // If the query includes `skipRead`, early return an object with primaryKey as a property
          if (options?.skipRead) return { [primaryKey]: parent[foreignKey] }

          const session =
            component.modelRegistry.sessions.getFromContext(context)
          const result = await component
            .getModel(`${model}`)
            .get({ ...params, ...options, ...session })

          if (Array.isArray(result)) {
            return component.isList(field.type) ? result : result[0]
          }
          return result
        }
      }
    }
  },
  isList(type) {
    return (
      type instanceof GraphQLList || (type.ofType && this.isList(type.ofType))
    )
  },
  isConnectionType(type) {
    return (
      (Array.isArray(type._interfaces) &&
        type._interfaces.find((i) => i.name === 'Connection')) ||
      (type.ofType && this.isConnectionType(type.ofType))
    )
  },
  isConnectionResult(res) {
    return res && Array.isArray(res.items)
  },

  getModel(name) {
    const model = this.modelRegistry[`${name}`]
    if (!model)
      throw Error(`Model connector error: no model registered as ${name}Model`)
    return model
  },
}
