const { AuthenticationError } = require('apollo-server')

module.exports = {
  name: 'authorisation',

  AUTHORISATION_ENUMS_SUFFIX: 'Operations',

  DEFAULT_OPERATIONS: {
    __schema: 'PublicOperations',
    IntrospectionQuery: 'PublicOperations',
  },

  registerAuthorisationGroups(schema) {
    this.authorisationGroups = Object.entries(schema.getTypeMap())
      .filter(([key]) =>
        new RegExp(`(${this.AUTHORISATION_ENUMS_SUFFIX})$`).test(key)
      )
      .reduce((acc, [groupName, GQLEnum]) => {
        GQLEnum.getValues().map(({ value }) => {
          acc[value] = groupName
        })
        return acc
      }, this.DEFAULT_OPERATIONS)
  },

  /**
   * Authorise graphql request
   * @param {*} options.operationName: name of query or mutation the user requested
   * @param {*} options.context: req, res and user as resolved by the session model
   */
  authorise({ operationName, context: { req, res, user } }) {
    // All operations are LoggedInUserOperations if they are not public
    const authGroup =
      this.authorisationGroups[operationName] || 'LoggedInUserOperations'
    const meetsCriteria = this[`meets${authGroup}`]({
      res,
      req,
      user,
      operationName,
    })
    if (!meetsCriteria)
      throw new AuthenticationError('User is not allowed to run this operation')
  },

  meetsPublicOperations: () => true,
  meetsLoggedInUserOperations: ({ user }) => !!user,
}
