const { AuthenticationError, UserInputError } = require('apollo-server')
const Filter = require('bad-words')
const { BASE_URL } = require('../../../config/config')
const profanityFilter = new Filter()

module.exports = {
  name: 'chatsModel',
  deps: ['modelRegistry', 'resourceModel', 'emails', 'chatsSubscriptions'],
  collection: 'chats',

  init() {
    this.modelRegistry.register('chats', this)
    this.MESSAGES_LIMIT = 100
  },

  async create({ isPublic, users, user }) {
    if (['undefined', 'null'].includes(typeof isPublic))
      throw new UserInputError('Cannot create a new chat without isPublic')
    if (isPublic && !user.isAdmin)
      throw new AuthenticationError(
        'User is not allowed to run this operation.'
      )
    return this.resourceModel.create({
      collection: this.collection,
      user,
      obj: {
        isPublic,
        users,
        messages: [],
      },
    })
  },

  async get({ id, user }) {
    return this.resourceModel.get({
      collection: this.collection,
      query: this.chatQuery({ id, userId: user.id }),
      formatter: this.formatter,
    })
  },

  async delete({ id }) {
    return this.resourceModel.update({
      collection: this.collection,
      id,
      update: {
        deleted: true,
      },
    })
  },

  async addParticipant({ id, user }) {
    return this.resourceModel.pushUpdate({
      query: {
        id,
        isPublic: false,
      },
      collection: this.collection,
      obj: {
        users: user.id,
      },
    })
  },

  async sendMessage({ id, message, user }) {
    const updatedChat = await this.resourceModel.pushUpdate({
      query: this.chatQuery({ id, userId: user.id }),
      collection: this.collection,
      formatter: this.formatter,
      obj: {
        messages: {
          from: user.id,
          publicName: user.username,
          message,
          sentAt: new Date(),
        },
      },
    })
    if (!updatedChat)
      throw UserInputError(
        `User ${user.id} is not allowed to publish to chat ${id}`
      )
    this.chatsSubscriptions.publish({
      chat: updatedChat,
      message: updatedChat.messages.slice(-1)[0],
    })
    if (!updatedChat.isPublic) this.notifyOfflineUsers({ chat: updatedChat })
    return updatedChat
  },

  /**
   * Sends an email to users who signed up for receiving offline messages, provided they are opted in to notifications
   * @param {Chat} chat: the chat updated with the last sent message.
   */
  async notifyOfflineUsers({ chat: { id, messages, users } }) {
    const [lastMessage] = messages.slice(-1)
    const { publicName, message, from, sentAt } = lastMessage
    const otherUsers = users.filter((itm) => itm !== from)
    const usersToNotify = (
      await this.modelRegistry.users.get({ ids: otherUsers })
    )
      .filter((itm) => itm.preferences.emailWhenMessageReceivedOffline)
      .filter((user) => !this.chatsSubscriptions.isChatUserOnline({ id, user }))
    if (!usersToNotify.length) return

    const [game] = await this.modelRegistry.games.get({ chat: id })
    usersToNotify.map(async (userToNotify) =>
      this.emails.send({
        to: userToNotify.email,
        template: 'MessageReceivedWhileOffline',
        subject: `${publicName} sent you message`,
        variables: {
          publicName,
          received: `${sentAt
            .toISOString()
            .split('T')[0]
            .split('-')
            .reverse()
            .join('/')} at ${sentAt.toISOString().split('T')[1].split('.')[0]}`,
          message,
          gameLink: `${BASE_URL}/play/${game.id}`,
        },
      })
    )
  },

  /**
   * Formatter for chat object
   * @param {Chat} chat: chat object
   */
  formatter({ messages, ...itm }) {
    return {
      messages: messages.slice(-this.MESSAGES_LIMIT).map(this.formatMessage),
      ...itm,
    }
  },

  /**
   * Formats message object
   * @param {object} message: message object with from and message property
   * @returns
   */
  formatMessage({ message, ...msg }) {
    try {
      return { ...msg, message: profanityFilter.clean(message) }
    } catch (e) {
      // Using a single emoji may break the filter
      return { ...msg, message: profanityFilter.placeHolder }
    }
  },

  chatQuery: ({ id, userId }) => ({
    id,
    $or: [
      {
        isPublic: false,
        users: userId,
      },
      {
        isPublic: true,
      },
    ],
  }),
}
