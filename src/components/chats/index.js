module.exports = {
  name: 'chats',
  deps: ['graphql', 'chatsModel', 'chatsSubscriptions'],

  init() {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('chats.graphql'),
      resolvers: {
        Mutation: {
          sendMessage: this.sendMessage,
          createChat: this.createChat,
        },
        Subscription: {
          messageSent: {
            subscribe: this.subscribeChat,
          },
        },
      },
    })
  },

  async createChat(parent, { isPublic, users }, { user }) {
    return this.chatsModel.create({ isPublic, users, user })
  },

  async sendMessage(parent, { id, message }, { user }) {
    return this.chatsModel.sendMessage({ id, message, user })
  },

  async subscribeChat(root, { id }, { user }) {
    return this.chatsSubscriptions.subscribe({ id, user })
  },
}
