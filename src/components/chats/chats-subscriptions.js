const { UserInputError } = require('apollo-server')

module.exports = {
  name: 'chatsSubscriptions',
  deps: ['pubsub', 'modelRegistry'],

  channel: ({ id, user }) => `CHAT_${id}_${user}`,

  /**
   * Looks up chat and subscribes user
   * @param {string} options.id: the chat id
   * @param {object} options.user: the currently signed in user
   */
  async subscribe({ id, user }) {
    const [chat] = await this.modelRegistry.chats.get({ id, user })
    if (!chat)
      throw UserInputError(
        `User ${user.id} is not allowed to publish to chat ${id}`
      )
    return this.pubsub.subscribe(this.channel({ id, user: user.id }))
  },

  /**
   * @returns whether a chat user is listening on a chat channel
   * @param {string} options.id: chat id
   * @param {string} options.user: user id to check
   */
  isChatUserOnline({ id, user }) {
    return this.pubsub.hasSubscribers(this.channel({ id, user: user.id }))
  },

  /**
   * Publishes message on relevant channels
   * @param {object} options.chat: The chat object
   * @param {object} options.message: The message to be published
   */
  async publish({ chat: { id }, message }) {
    const channels = this.pubsub.matchingChannels(`CHAT_${id}`)

    return channels.forEach((channel) =>
      this.pubsub.publishMessage({
        channel,
        message: { messageSent: message },
      })
    )
  },
}
