// prettier-ignore
(async () => {
  const start = new Date()
  const madhouse = require('madhouse')
  const container = await madhouse.loadContainer()
  container.server.startServer()
  console.log(`Container started in ${new Date() - start} ms`)
})()
