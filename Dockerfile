# Container for app
FROM node:12

COPY . /opt/app
WORKDIR /opt/app
RUN yarn

EXPOSE 4001

CMD yarn watch

