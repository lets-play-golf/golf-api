mongo -- "$MONGO_INITDB_DATABASE" <<EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = '$GOLF_API_MONGO_USERNAME';
    var passwd = '$GOLF_API_MONGO_PASSWORD';
    db.createUser({user: user, pwd: passwd, roles: [ { role: 'root', db: 'admin' } ]});
EOF

mongo -- "$MONGO_INITDB_DATABASE-test" <<EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = '$GOLF_API_MONGO_USERNAME';
    var passwd = '$GOLF_API_MONGO_PASSWORD';
    db.createUser({user: user, pwd: passwd, roles: [ { role: 'root', db: 'admin' } ]});
EOF

