#!/bin/bash
port=4001
ENV=${1:-test}
kill -INT $(lsof -nPti tcp:$port)
echo "Run tests with env: ${ENV}"
npx nyc npm run start:${ENV} &
PID=$!
while ! nc -z localhost $port
do
  echo "Waiting for gateway to be up on port $port"
  sleep 0.5
done
yarn nyc --clean yarn test-integration:${ENV}
TEST_RESULT=$?
newPid=$(lsof -nPti tcp:$port)
echo "Tests finished with exit code ${TEST_RESULT}, killing PID ${PID} newPid ${newPid}"
kill ${PID}
kill ${newPid}
while nc -z localhost $port
do
  echo "Gateway still running!"
  sleep 0.5
done
wait ${PID}
echo "Wait finished"
exit $TEST_RESULT