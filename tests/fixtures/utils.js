module.exports = {
  transformMongoToDataObject: (list) => {
    return list.reduce(
      (acc, { _id, ...itm }) => [...acc, { id: _id?.$oid, ...itm }],
      []
    )
  },
}
