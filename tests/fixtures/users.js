const { transformMongoToDataObject } = require('./utils')
const users = transformMongoToDataObject(require('./users/users.json'))

const player1 = {
  ...users[0],
  password: 'secret',
}

const player2 = {
  ...users[2],
  password: 'secret',
}

const player3 = {
  ...users[3],
  password: 'secret',
}

const bots = {
  randobot: users[4],
}

module.exports = {
  activeUsers: [player1, player2, player3],
  activeUser: player1,
  inactiveUser: users[1],
  bots,
}
