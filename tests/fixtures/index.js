const users = require('./users')
const games = require('./games')
const userActivity = require('./user-activity')

module.exports = {
  users,
  games,
  userActivity,
}
