const { transformMongoToDataObject } = require('./utils')
const games = transformMongoToDataObject(require('./games/games.json'))

module.exports = {
  openGames: [games[0]],
  startedGames: [games[1]],
  playGames: [games[2]],
}
