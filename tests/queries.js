const createSessionQuery = `
  mutation createSession ($email: String!, $password: String!) {
    createdSession: createSession(email: $email, password: $password) {
      ...on Session {
        user {
          id,
          firstname,
          email,
          createdAt,
          deleted
        }
      }
      ...on UserInputError {
        error {
          message
          key
          fields
        }
      }
    }
  }
`

module.exports = { createSessionQuery }
