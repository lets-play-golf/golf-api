const GraphqlClient = require('./utilities/graphql-client')

const { mongoQuery, closeMongo } = require('./utilities/dbUtilities')
const { resetCollection } = require('./utilities/dbSeed')
const { activeUsers } = require('./fixtures/users')

const player = activeUsers[2]

const requestMagicLink = `
  mutation requestMagicLink($email: String!) {
    requestMagicLink(email: $email)
  }
`

describe('requestMagicLink mutation', () => {
  afterAll(closeMongo)

  it('should create a reset hash for an existing user', async () => {
    const client = await new GraphqlClient()

    const { data, error } = await client.request({
      query: requestMagicLink,
      variables: {
        email: player.email,
      },
    })

    expect(error).toBeUndefined()
    expect(data.requestMagicLink).toBe(true)

    const [[{ hash }]] = await mongoQuery({
      collection: 'user-activity',
      query: {
        email: player.email,
        type: 'MagicLinkRequest',
      },
    })
    expect(hash.length).toBe(124)
  })

  it('should not create a reset hash for an existing user', async () => {
    const client = await new GraphqlClient()

    const { data, error } = await client.request({
      query: requestMagicLink,
      variables: {
        email: 'account_not_found@gmail.com',
      },
    })

    expect(error).toBeUndefined()
    expect(data.requestMagicLink).toBe(true)

    const [response] = await mongoQuery({
      collection: 'user-activity',
      query: {
        email: 'account_not_found@gmail.com',
        type: 'MagicLinkRequest',
      },
    })
    expect(response.length).toBe(0)
  })
})

describe('checkMagicLink mutation', () => {
  afterAll(closeMongo)
  beforeEach(async () => resetCollection({ id: 'user-activity' }))
  afterAll(async () => resetCollection({ id: 'user-activity' }))

  const checkMagicLink = `
    mutation checkMagicLink($hash: String!){
      checkMagicLink(hash: $hash) 
    }
  `
  it('should return the time left to use a valid link', async () => {
    const client = await new GraphqlClient()

    const requestLink = await client.request({
      query: requestMagicLink,
      variables: {
        email: player.email,
      },
    })

    expect(requestLink.error).toBeUndefined()

    const [[{ hash }]] = await mongoQuery({
      collection: 'user-activity',
      query: {
        email: player.email,
        type: 'MagicLinkRequest',
      },
    })

    const { data, error } = await client.request({
      query: checkMagicLink,
      variables: { hash },
    })

    expect(error).toBeUndefined()

    const timeLeftInMins = data.checkMagicLink
    expect(timeLeftInMins > 0).toBe(true)
  })

  it(`should return 0 if the link doesn't exist`, async () => {
    const client = await new GraphqlClient()

    const { data, error } = await client.request({
      query: checkMagicLink,
      variables: {
        hash: 'hash_doesnt_exist',
      },
    })

    expect(error).toBeUndefined()

    expect(data.checkMagicLink).toBe(0)
  })
})

describe('consumeMagicLink mutation', () => {
  afterAll(closeMongo)
  beforeEach(async () => resetCollection({ id: 'user-activity' }))

  const consumeMagicLink = `
    mutation consumeMagicLink($hash: String!) {
      consumeMagicLink(hash: $hash) {
        ...on Session {
          user {
            id
            firstname
            email
            username
          }
        }
        ...on UserInputError {
          error {
            message
            key
            fields
          }
        }
      }
    }
  `
  it('should log in the user if the link is valid', async () => {
    const client = await new GraphqlClient()

    const requestLink = await client.request({
      query: requestMagicLink,
      variables: {
        email: player.email,
      },
    })

    expect(requestLink.error).toBeUndefined()

    const [[{ hash }]] = await mongoQuery({
      collection: 'user-activity',
      query: {
        email: player.email,
        type: 'MagicLinkRequest',
      },
    })

    const { data, error } = await client.request({
      query: consumeMagicLink,
      variables: {
        hash,
      },
    })

    expect(error).toBeUndefined()
    const {
      consumeMagicLink: {
        user: { id, email, username, firstname },
      },
    } = data
    expect(typeof id).toBe('string')
    expect(typeof email).toBe('string')
    expect(typeof username).toBe('string')
    expect(typeof firstname).toBe('string')
    expect(email).toBe(player.email)

    const me = await client.me()

    expect(me.error).toBeUndefined()

    expect(me.data.me.email).toBe(player.email)
  })

  it('should not log in the user if the link is invalid', async () => {
    const client = await new GraphqlClient()

    const { data, error } = await client.request({
      query: consumeMagicLink,
      variables: {
        hash: 'invalid_hash',
      },
    })

    expect(error).toBeUndefined()

    const {
      consumeMagicLink: {
        error: { key, message },
      },
    } = data
    expect(key).toBe('EXPIRED_MAGIC_LINK')
    expect(message).toBe('Magic link is expired')
  })

  it('should not log in the user if the link has already been used', async () => {
    const firstClient = new GraphqlClient()

    const requestLink = await firstClient.request({
      query: requestMagicLink,
      variables: {
        email: player.email,
      },
    })
    expect(requestLink.error).toBeUndefined()

    const [[{ hash }]] = await mongoQuery({
      collection: 'user-activity',
      query: {
        email: player.email,
        type: 'MagicLinkRequest',
      },
    })

    const firstAttempt = await firstClient.request({
      query: consumeMagicLink,
      variables: {
        hash,
      },
    })

    expect(firstAttempt.error).toBeUndefined()

    // Reuse the link
    const secondClient = new GraphqlClient()

    const secondAttempt = await secondClient.request({
      query: consumeMagicLink,
      variables: {
        hash,
      },
    })

    expect(secondAttempt.error).toBeUndefined()

    const {
      consumeMagicLink: {
        error: { key, message },
      },
    } = secondAttempt.data
    expect(key).toBe('EXPIRED_MAGIC_LINK')
    expect(message).toBe('Magic link is expired')

    const me = await secondClient.me()

    expect(
      me.error.message.startsWith('User is not allowed to run this operation')
    ).toBe(true)
  })
})
