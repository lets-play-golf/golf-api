const {
  users: { activeUser, inactiveUser },
} = require('./fixtures')
const GraphqlClient = require('./utilities/graphql-client')
const { resetCollection, resetDB } = require('./utilities/dbSeed')
const { createSessionQuery } = require('./queries')

beforeAll(resetDB)

describe('Sessions model', () => {
  describe('createSession mutation', () => {
    it('should allow active users to create a session', async () => {
      const variables = {
        email: activeUser.email,
        password: activeUser.password,
      }
      const resp = await new GraphqlClient().request({
        query: createSessionQuery,
        variables,
      })
      const {
        data: { createdSession, error },
      } = resp
      const { user } = createdSession
      expect(error).toBe(undefined)
      expect(user.email).toBe(activeUser.email)
      expect(user.firstname).toBe(activeUser.firstname)
    })

    it('should allow the user to access their information once they are logged in', async () => {
      const client = await new GraphqlClient().createSession(activeUser)
      const {
        data: {
          me: { email },
        },
      } = await client.request({ query: 'query { me { email } }' })
      expect(email).toBe(activeUser.email)
    })

    it('should disallow existing users to create a session with a wrong password', async () => {
      const variables = {
        email: 'rupaul@potus.com',
        password: 'Inc0rrect_password',
      }
      const {
        data: {
          createdSession: { error },
        },
      } = await new GraphqlClient().request({
        query: createSessionQuery,
        variables,
      })
      expect(error.key).toBe('WRONG_CREDENTIALS')
    })

    it('should disallow inactive users to create a session', async () => {
      const variables = {
        email: inactiveUser.email,
        password: 'secret',
      }
      const {
        data: {
          createdSession: {
            error: { key, message },
          },
        },
      } = await new GraphqlClient().request({
        query: createSessionQuery,
        variables,
      })
      expect(typeof key).toBe('string')
      expect(typeof message).toBe('string')
    })

    it('should prevent login attempts after the 3rd wrong attempt', async () => {
      await resetCollection({ id: `user-activity` })
      const client = new GraphqlClient()
      const variables = {
        email: 'rupaul@potus.com',
        password: 'Inc0rrect_password',
      }
      const firstAttempt = await client.request({
        query: createSessionQuery,
        variables,
      })
      expect(firstAttempt.data.createdSession.error.key).toBe(
        'WRONG_CREDENTIALS'
      )
      const secondAttempt = await client.request({
        query: createSessionQuery,
        variables,
      })
      expect(secondAttempt.data.createdSession.error.key).toBe(
        'WRONG_CREDENTIALS'
      )
      const thirdAttempt = await client.request({
        query: createSessionQuery,
        variables,
      })
      expect(thirdAttempt.data.createdSession.error.key).toBe(
        'WRONG_CREDENTIALS'
      )
      const fourthAttempt = await client.request({
        query: createSessionQuery,
        variables,
      })
      expect(fourthAttempt.data.createdSession.error.key).toBe('ACCOUNT_LOCKED')
    })
  })
})
