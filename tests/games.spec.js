const GraphqlClient = require('./utilities/graphql-client')
const { resetDB } = require('./utilities/dbSeed')
const {
  users: {
    activeUsers,
    bots: { randobot },
  },
  games: {
    openGames: [openGame],
    startedGames: [startedGame],
  },
} = require('./fixtures')

beforeEach(resetDB)

const [player1, player2] = activeUsers

describe('Games model', () => {
  describe('createGame mutation', () => {
    const query = `
    mutation createGame($numberOfRounds: Int!) {
      createdGame: createGame (
        numberOfRounds: $numberOfRounds
      ) {
        id
        numberOfRounds
        players {
          user {
            id
            username
          }
          score
          isOnline
        }
        status
        createdAt
        createdBy {
          id
          firstname
          username
        }
      }
    }`
    it('should allow a registered user to create a game', async () => {
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const variables = {
        numberOfRounds: 5,
      }
      const {
        data: { createdGame },
        error,
      } = await client.request({ query, variables })
      expect(error).toBeUndefined()
      expect(typeof createdGame.id).toBe('string')
      expect(typeof createdGame.numberOfRounds).toBe('number')
      expect(typeof createdGame.createdAt).toBe('string')

      expect(createdGame.players[0]).toEqual(
        expect.objectContaining({
          user: expect.objectContaining({ id: expect.stringContaining('') }),
          score: [],
        })
      )
      expect(createdGame.status).toBe('created')
      expect(createdGame.numberOfRounds).toBe(5)
    })
  })

  describe('joinGame mutation', () => {
    const query = `
    mutation joinGame($id: ID!) {
      joinedGame: joinGame (id: $id) {
        id
        numberOfRounds
        players {
          score
        }
        status
        createdAt
      }
    }`
    it('should add the user as a player to a game', async () => {
      const client = await new GraphqlClient().createSession({
        email: player2.email,
        password: player2.password,
      })
      const variables = { id: openGame.id }
      const query1 = await client.request({ query, variables })
      expect(query1.error).toBeUndefined()
      expect(query1.data.joinedGame.players.length).toBe(2)

      // User cannot join the same game again
      const query2 = await client.request({ query, variables })
      expect(query2.error).not.toBeUndefined()
      expect(query2.error.message).toContain(
        `User already joined game ${openGame.id}`
      )
    })
    it('should prevent the user to join a started game', async () => {
      const client = await new GraphqlClient().createSession({
        email: player2.email,
        password: player2.password,
      })
      const variables = { id: startedGame.id }
      const { error } = await client.request({ query, variables })
      expect(error).not.toBeUndefined()
      expect(error.message).toContain(`Game ${startedGame.id} cannot be joined`)
    })
  })

  describe('addPlayer', () => {
    it('should allow a user to add a bot only once', async () => {
      const query = `
        mutation addABot ($id: ID! $bot: ID!) {
          updatedGame: addABot(
            id: $id
            bot: $bot
          ) {
            players {
              userId
            }
          }
        }
      `
      const variables = {
        id: openGame.id,
        bot: randobot.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const { data, error } = await client.request({ query, variables })
      expect(error).toBeUndefined()
      expect(
        data.updatedGame.players.some(({ userId }) => userId === randobot.id)
      ).toBe(true)

      const secondAttempt = await client.request({ query, variables })
      expect(secondAttempt.error.message).toContain(
        'Could not add bot to this game'
      )
    })

    it('should not allow a user that is not on the game to add the bot', async () => {
      const query = `
        mutation addABot ($id: ID! $bot: ID!) {
          updatedGame: addABot(
            id: $id
            bot: $bot
          ){
            players {
              userId
            }
          }
        }
      `
      const variables = {
        id: openGame.id,
        bot: randobot.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player2.email,
        password: player2.password,
      })
      const { error } = await client.request({ query, variables })
      expect(error.message).toContain(
        `User ${player2.id} is not allowed to access this game`
      )
    })

    it('should not allow for an invalid bot id to be added', async () => {
      const query = `
        mutation addABot ($id: ID! $bot: ID!) {
          updatedGame: addABot(
            id: $id
            bot: $bot
          ){
            players {
              userId
            }
          }
        }
      `
      const variables = {
        id: openGame.id,
        bot: '10101',
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const { error } = await client.request({ query, variables })
      expect(error.message).toContain('Could not find bot 10101')
    })
  })

  describe('start game', () => {
    const addPlayer = async () => {
      const query = `
        mutation addABot ($id: ID! $bot: ID!) {
          updatedGame: addABot(
            id: $id
            bot: $bot
          ) {
            players {
              userId
            }
          }
        }
      `
      const variables = {
        id: openGame.id,
        bot: randobot.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      return client.request({ query, variables })
    }
    it('should allow a game with more than 1 user to be started', async () => {
      await addPlayer()
      const query = `
        mutation startGame ($id: ID!) {
          startedGame: startGame(id: $id) {
            id
            status
          }
        }
      `
      const variables = {
        id: openGame.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const { data, error } = await client.request({ query, variables })
      expect(error).toBeUndefined()
      expect(data.startedGame.status).toBe('started')
    })
    it('should not allow a game with less than 2 players to be started', async () => {
      const query = `
        mutation startGame ($id: ID!) {
          startedGame: startGame(id: $id) {
            id
            status
          }
        }
      `
      const variables = {
        id: openGame.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const { error } = await client.request({ query, variables })
      expect(error.message).toContain(
        'Cannot start the game as there is not enough players'
      )
    })
    it('should not allow a game that is already started to be started', async () => {
      const query = `
        mutation startGame ($id: ID!) {
          startedGame: startGame(id: $id) {
            id
            status
          }
        }
      `
      const variables = {
        id: startedGame.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player1.email,
        password: player1.password,
      })
      const { error } = await client.request({ query, variables })
      expect(error.message).toContain('Cannot start the game')
    })
    it('should not allow a user not registered on the game to start the game', async () => {
      const query = `
        mutation startGame ($id: ID!) {
          startedGame: startGame(id: $id) {
            id
            status
          }
        }
      `
      const variables = {
        id: openGame.id,
      }
      const client = await new GraphqlClient().createSession({
        email: player2.email,
        password: player2.password,
      })
      const { error } = await client.request({ query, variables })
      expect(error.message).toContain(
        `User ${player2.id} is not allowed to access this game`
      )
    })
  })

  describe('update game mutation', () => {
    describe('update numberOfRounds', () => {
      it('should allow the user to update the number of rounds', async () => {
        const query = `
          mutation updateGame($id: ID! $update: GameUpdate!) {
            updatedGame: updateGame (
              id: $id
              update: $update
            ) {
              id
              numberOfRounds
            }
          }
        `
        const variables = {
          id: openGame.id,
          update: {
            numberOfRounds: 10,
          },
        }
        const client = await new GraphqlClient().createSession({
          email: player1.email,
          password: player1.password,
        })
        const { data, error } = await client.request({ query, variables })
        expect(error).toBeUndefined()
        expect(data.updatedGame.numberOfRounds).toBe(10)
      })
    })
  })
})
