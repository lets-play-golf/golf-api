const GraphqlClient = require('./utilities/graphql-client')
const { resetCollection } = require('./utilities/dbSeed')
const { mongoQuery, closeMongo } = require('./utilities/dbUtilities')
const { createSessionQuery } = require('./queries')

beforeEach(async () => resetCollection({ ids: ['users', 'user-activity'] }))
afterAll(closeMongo)

describe('resetPassword mutation', () => {
  const requestResetPassword = `mutation requestPasswordReset($email: String!){
    requestPasswordReset(email: $email)
  }`
  const resetPassword = `mutation resetPassword($password: String! $resetHash: String!){
    resetPassword(password: $password, resetHash: $resetHash)
  }`

  it('should allow a user to reset their password', async () => {
    const client = await new GraphqlClient()

    // request password reset
    const requestPassReset = await client.request({
      query: requestResetPassword,
      variables: { email: 'rupaul@potus.com' },
    })
    expect(requestPassReset.error).toBeUndefined()
    expect(requestPassReset.data.requestPasswordReset).toBe(true)

    // reset password
    const [response] = await mongoQuery({
      collection: 'user-activity',
      query: { type: 'PasswordResetRequested' },
    })
    const [{ resetHash }] = response
    const resetPass = await client.request({
      query: resetPassword,
      variables: {
        resetHash,
        password: 'P@ssword1',
      },
    })
    expect(resetPass.errors).toBeUndefined()
    expect(resetPass.data.resetPassword).toBe(true)

    // createSession
    const createSession = await client.request({
      query: createSessionQuery,
      variables: {
        email: 'rupaul@potus.com',
        password: 'P@ssword1',
      },
    })
    expect(createSession.error).toBeUndefined()
    expect(createSession.data.createdSession.user.email).toBe(
      'rupaul@potus.com'
    )
  })

  it("should not allow a user to reset their password if they don't use the latest reset code", async () => {
    const client = await new GraphqlClient()

    // request password reset
    const fistRequestPassReset = await client.request({
      query: requestResetPassword,
      variables: { email: 'rupaul@potus.com' },
    })
    const secondRequestPassReset = await client.request({
      query: requestResetPassword,
      variables: { email: 'rupaul@potus.com' },
    })

    expect(fistRequestPassReset.error).toBeUndefined()
    expect(secondRequestPassReset.error).toBeUndefined()

    // 1st reset password should fail
    const [[{ resetHash: secondResetHash }, { resetHash: firstResetHash }]] =
      await mongoQuery({
        collection: 'user-activity',
        query: { type: 'PasswordResetRequested' },
        sort: { _id: -1 },
        limit: 2,
      })
    const firstResetPass = await client.request({
      query: resetPassword,
      variables: {
        resetHash: firstResetHash,
        password: 'P@ssword1',
      },
    })
    expect(firstResetPass.error).toBeUndefined()
    expect(firstResetPass.data.resetPassword).toBe(false)

    // 2nd reset password should work
    const secondResetPass = await client.request({
      query: resetPassword,
      variables: {
        resetHash: secondResetHash,
        password: 'P@ssword1',
      },
    })
    expect(secondResetPass.error).toBeUndefined()
    expect(secondResetPass.data.resetPassword).toBe(true)
  })

  it("should unlock the user's account so that the user can log in again", async () => {
    const client = await new GraphqlClient()
    await client.request({
      query: createSessionQuery,
      variables: { email: 'rupaul@potus.com', password: 'Wr0ngPassword' },
    })
    await client.request({
      query: createSessionQuery,
      variables: { email: 'rupaul@potus.com', password: 'Wr0ngPassword' },
    })
    await client.request({
      query: createSessionQuery,
      variables: { email: 'rupaul@potus.com', password: 'Wr0ngPassword' },
    })
    const fourthAttempt = await client.request({
      query: createSessionQuery,
      variables: { email: 'rupaul@potus.com', password: 'Wr0ngPassword' },
    })
    expect(fourthAttempt.data.createdSession.error.key).toBe('ACCOUNT_LOCKED')

    // request password reset
    const requestPassReset = await client.request({
      query: requestResetPassword,
      variables: { email: 'rupaul@potus.com' },
    })
    expect(requestPassReset.error).toBeUndefined()
    expect(requestPassReset.data.requestPasswordReset).toBe(true)
    // Account still locked
    const createSessionFails = await client.request({
      query: createSessionQuery,
      variables: {
        email: 'rupaul@potus.com',
        password: 'P@ssword1',
      },
    })
    expect(createSessionFails.data.createdSession.error.key).toBe(
      'ACCOUNT_LOCKED'
    )

    // reset password
    const [[{ resetHash }]] = await mongoQuery({
      collection: 'user-activity',
      query: { type: 'PasswordResetRequested' },
    })
    const resetPass = await client.request({
      query: resetPassword,
      variables: {
        resetHash,
        password: 'P@ssword1',
      },
    })
    expect(resetPass.errors).toBeUndefined()
    expect(resetPass.data.resetPassword).toBe(true)

    // createSession
    const createSessionSuccess = await client.request({
      query: createSessionQuery,
      variables: {
        email: 'rupaul@potus.com',
        password: 'P@ssword1',
      },
    })
    expect(createSessionSuccess.data.createdSession.error).toBeUndefined()
    expect(createSessionSuccess.data.createdSession.user.email).toBe(
      'rupaul@potus.com'
    )
  })
})
