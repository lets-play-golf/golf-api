const { Seeder } = require('mongo-seeding')
const path = require('path')

/**
 * Config - @see https://www.npmjs.com/package/mongo-seeding?activeTab=readme
 */
const {
  MONGO: {
    test: {
      database: name,
      hosts: [{ host, port }],
      username,
      password,
    },
  },
} = require('../../app.env.json')

const seeder = new Seeder({
  database: { host, port: +port, name, username, password },
  dropCollections: true,
})

// Collections - folders of `./tests/fixtures`
const collections = seeder.readCollectionsFromPath(
  path.resolve('./tests/fixtures')
)

module.exports = {
  /**
   * Reset all mongo db collections in ../data folders
   */
  resetDB: async () => seeder.import(collections),

  /**
   * Reset specific collections
   * @param {*} options.id: collection id matching folder in ../data
   * @param {*} options.ids: collection ids matching folders in ../data
   */
  resetCollection: async ({ id, ids = [] } = {}) => {
    if (id) ids.push(id)
    return seeder.import(collections.filter(({ name }) => ids.includes(name)))
  },
}
