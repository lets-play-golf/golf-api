#!/usr/bin/env bash
HOST=${1:-localhost:3000}
DB=${2}

mongo $DB --host $HOST \
    --eval 'db.getCollection("user-activity").drop()'