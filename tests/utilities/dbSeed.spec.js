const { resetDB, resetCollection } = require('./dbSeed')
const GraphqlClient = require('./graphql-client')
const client = new GraphqlClient()
const {
  users: { activeUsers },
  games: {
    openGames: [openGame],
  },
} = require('./../fixtures')
const player2 = activeUsers[1]

describe('resetDB', () => {
  it('should reset the users collection', async () => {
    await resetDB()
    const hasNewUserRegisteredSuccessfully = await registerUser()
    expect(hasNewUserRegisteredSuccessfully).toBe(true)
    await resetDB()

    const hasSameUserRegisteredSuccessfully = await registerUser()
    expect(hasSameUserRegisteredSuccessfully).toBe(true)
  })

  it('should reset the games collection', async () => {
    await resetDB()
    const hasNewJoinedGameSuccessfully = await joinGame()
    expect(hasNewJoinedGameSuccessfully).toBe(true)
    await resetDB()

    const hasSameUserJoinedGameSuccessfully = await joinGame()
    expect(hasSameUserJoinedGameSuccessfully).toBe(true)
  })
  it.todo('should reset the user-activity')

  it.todo('should reset the chats collection')
})

describe('resetCollection', () => {
  describe('single id', () => {
    it('should reset the users collection', async () => {
      await resetCollection({ id: 'users' })
      const hasNewUserRegisteredSuccessfully = await registerUser()
      expect(hasNewUserRegisteredSuccessfully).toBe(true)
      await resetCollection({ id: 'users' })

      const hasSameUserRegisteredSuccessfully = await registerUser()
      expect(hasSameUserRegisteredSuccessfully).toBe(true)
    })

    it('should reset the games collection', async () => {
      await resetCollection({ id: 'games' })
      const hasNewJoinedGameSuccessfully = await joinGame()
      expect(hasNewJoinedGameSuccessfully).toBe(true)
      await resetCollection({ id: 'games' })

      const hasSameUserJoinedGameSuccessfully = await joinGame()
      expect(hasSameUserJoinedGameSuccessfully).toBe(true)
    })
    it.todo('should reset the user-activity')

    it.todo('should reset the chats collection')
  })

  describe('multiple ids', () => {
    it('should reset both users and games collections', async () => {
      await resetCollection({ ids: ['users', 'games'] })
      const hasNewUserRegisteredSuccessfully = await registerUser()
      expect(hasNewUserRegisteredSuccessfully).toBe(true)

      const hasNewJoinedGameSuccessfully = await joinGame()
      expect(hasNewJoinedGameSuccessfully).toBe(true)
      await resetCollection({ ids: ['users', 'games'] })

      const hasSameUserRegisteredSuccessfully = await registerUser()
      expect(hasSameUserRegisteredSuccessfully).toBe(true)

      const hasSameUserJoinedGameSuccessfully = await joinGame()
      expect(hasSameUserJoinedGameSuccessfully).toBe(true)
    })
    it.todo('should reset the user-activity')

    it.todo('should reset the chats collection')
  })
})

const registerUser = async () => {
  const query = `
      mutation createUser ($email: String!, $firstname: String!, $username: String!, $password: String!, $emailWhenInvitationAccepted: Boolean!, $emailWhenMessageReceivedOffline: Boolean!) {
        session: createUser (email: $email, firstname: $firstname, username: $username, password: $password, emailWhenInvitationAccepted: $emailWhenInvitationAccepted, emailWhenMessageReceivedOffline: $emailWhenMessageReceivedOffline) {
          ...on Session {
          user {
            id
            firstname
            username
            email
            preferences {
              emailWhenInvitationAccepted
              emailWhenMessageReceivedOffline
            }
            createdAt
            deleted
          }
        }
        ...on UserInputError {
          error {
            key
            message
            fields
          }
        }
      }
    }`
  const variables = {
    email: 'shasha.velours@wow.com',
    username: 'shasha',
    firstname: 'Sasha',
    password: 'Str0ngPassword@1',
    emailWhenInvitationAccepted: true,
    emailWhenMessageReceivedOffline: false,
  }
  const {
    data: {
      session: { user },
    },
    error,
  } = await client.request({ query, variables })

  return !error && user?.email === variables.email
}

const joinGame = async () => {
  const query = `
  mutation joinGame($id: ID!) {
    joinedGame: joinGame (id: $id) {
      id
      numberOfRounds
      players {
        score
      }
      status
      createdAt
    }
  }`
  const client = await new GraphqlClient().createSession({
    email: player2.email,
    password: player2.password,
  })
  const variables = { id: openGame.id }
  const response = await client.request({ query, variables })
  return !response?.error
}
