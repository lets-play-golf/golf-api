#!/usr/bin/env bash

HOST=${1}
DB=${2}

# user-activity
mongo $DB --host $HOST \
    --eval 'db.getCollection("user-activity").drop()'

# users
mongo $DB --host $HOST \
    --eval 'db.users.drop()'

mongoimport --host $HOST \
    --db $DB --collection users --type JSON --file ./tests/fixtures/users.json --jsonArray