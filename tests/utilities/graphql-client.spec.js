const GraphqlClient = require('./graphql-client')
const { resetDB } = require('./dbSeed')
const { createSessionQuery } = require('../queries')
const {
  users: { activeUser },
} = require('../fixtures')

const user = {
  email: activeUser.email,
  password: activeUser.password,
}

beforeAll(resetDB)

describe('graphql-request client', () => {
  describe('request function', () => {
    it('should contain `data` when request is successful', async () => {
      const client = new GraphqlClient()
      const { data, error } = await client.request({
        query: createSessionQuery,
        variables: {
          email: user.email,
          password: user.password,
        },
      })
      if (error) throw new Error('Unexpected request error', error)
      expect(typeof data).toBe('object')
      expect(typeof data.createdSession).toBe('object')
    })
    it('should contain `error` when the request errors', async () => {
      const client = new GraphqlClient()
      const { error } = await client.request({
        query: `query me { email }`,
      })
      const { statusCode, message, errors } = error
      expect(statusCode).toEqual(400)
      expect(message).not.toBeUndefined()
      expect(Array.isArray(errors)).toBe(true)
      const [{ extensions, locations, message: msg }] = errors
      expect(extensions).not.toBe(undefined)
      expect(locations).not.toBe(undefined)
      expect(msg).not.toBe(undefined)
    })
  })
  describe('createSession query', () => {
    it('should log in the user', async () => {
      const client = await new GraphqlClient().createSession({
        email: user.email,
        password: user.password,
      })
      const resp = await client.me()
      const { data } = resp
      expect(data).toEqual({ me: { email: user.email } })
    })
  })

  describe('me query', () => {
    it('should initially error', async () => {
      const client = new GraphqlClient()
      const {
        error: { statusCode, message, errors },
      } = await client.me()
      expect(statusCode).toBe(400)
      expect(
        message.startsWith('User is not allowed to run this operation')
      ).toBe(true)
      expect(errors).toEqual([
        {
          extensions: { code: 'UNAUTHENTICATED' },
          message: 'User is not allowed to run this operation',
        },
      ])
    })

    it('should get the user`s id if the user is logged in', async () => {
      const client = await new GraphqlClient().createSession({
        email: user.email,
        password: user.password,
      })
      const { data } = await client.me()
      expect(data).toEqual({ me: { email: user.email } })
    })

    it('should not share session cookies with another client', async () => {
      const client1 = await new GraphqlClient().createSession({
        email: user.email,
        password: user.password,
      })
      const resp1 = await client1.me()
      expect(resp1.data).toEqual({ me: { email: user.email } })

      const client2 = new GraphqlClient()
      const resp2 = await client2.me()
      expect(resp2.data).toBe(undefined)
      expect(resp2.error).not.toBe(undefined)
    })
  })
})
