const { createComponent } = require('madhouse')

let mongo = null

const initMongo = async () => {
  if (!mongo) {
    mongo = createComponent(require('../../src/components/mongo'))
    await mongo.init()
  }
}

const mongoQuery = async ({
  collection,
  query,
  limit = 10,
  sort = { _id: 1 },
}) => {
  await initMongo()
  return Promise.all([
    mongo.db
      .collection(collection)
      .find(query)
      .limit(limit)
      .sort(sort)
      .toArray(),
  ])
}

const closeMongo = async () => {
  await mongo.client.close()
  mongo = null
}

module.exports = { mongoQuery, closeMongo }
