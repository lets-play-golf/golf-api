const { createSessionQuery } = require('../queries')
const { GraphQLClient, gql } = require('graphql-request')
const crossFetch = require('cross-fetch')

/**
 * Graphql client supporting session cookies scoped to the client object.
 */
class GraphqlClient {
  constructor() {
    const fetch = require('fetch-cookie')(crossFetch)
    this.client = new GraphQLClient('http://localhost:4001/graphql', { fetch })
  }

  /**
   * Sends a request to the graphql endpoint and returns data or error response
   * @param {string} options.query: graphql query or mutation
   * @param {object} options.variables: variables to be sent with query or mutation
   * @returns {data | error}
   */
  async request({ query, variables }) {
    try {
      const resp = await this.client.request(
        gql`
          ${query}
        `,
        variables
      )
      return { data: resp }
    } catch (e) {
      //throw new Error(e)
      return {
        error: {
          statusCode: e?.response?.status,
          message: e?.message,
          errors: e?.response?.errors,
        },
      }
    }
  }

  /**
   * Create user session - Use for logged in users only, otherwise it will error
   * @param {string} options.email: active user's email
   * @param {string} options.password: active user's password
   * @throws error if the credentials are wrong
   * @returns: The client with logged in session cookies
   */
  async createSession({ email, password }) {
    const {
      data: {
        createdSession: { error, user },
      },
    } = await this.request({
      query: createSessionQuery,
      variables: {
        email,
        password,
      },
    })
    if (!user && error) throw error
    return this
  }

  /**
   * Executes `me` query returning the user's email if they are logged in.
   */
  me() {
    return this.request({ query: 'query { me { email }}' })
  }
}
module.exports = GraphqlClient
