const GraphqlClient = require('./utilities/graphql-client')
const { resetDB } = require('./utilities/dbSeed')
const client = new GraphqlClient()
const {
  users: { activeUser },
} = require('./fixtures')

beforeEach(resetDB)

describe('Users model', () => {
  describe('me query', () => {
    const query = `query {
      me {
        id,
        email,
        username,
        firstname,
        preferences {
          emailWhenInvitationAccepted,
          emailWhenMessageReceivedOffline
        }, 
        createdAt, 
        deleted
      }
    }`

    it('should not expose user information before the user logs in', async () => {
      const { error } = await client.request({ query })
      expect(error.statusCode).toBe(400)
    })

    it('should expose user information once the user has logged in', async () => {
      await client.request({
        query: `mutation createSession ($email: String!, $password: String!) {
          createdSession: createSession(email: $email, password: $password) {
            ...on Session {
              user {
                id
              }
            }
          }
        }`,
        variables: {
          email: activeUser.email,
          password: activeUser.password,
        },
      })
      const {
        error,
        data: { me },
      } = await client.request({ query })
      if (error) throw error
      expect(typeof me.id).toBe('string')
      expect(typeof me.email).toBe('string')
      expect(typeof me.username).toBe('string')
      expect(typeof me.firstname).toBe('string')
      expect(typeof me.preferences).toBe('object')
      expect(typeof me.preferences.emailWhenInvitationAccepted).toBe('boolean')
      expect(typeof me.preferences.emailWhenMessageReceivedOffline).toBe(
        'boolean'
      )
    })
  })

  describe('createUser mutation', () => {
    const query = `
      mutation createUser ($email: String!, $firstname: String!, $username: String!, $password: String!, $emailWhenInvitationAccepted: Boolean!, $emailWhenMessageReceivedOffline: Boolean!) {
        session: createUser (email: $email, firstname: $firstname, username: $username, password: $password, emailWhenInvitationAccepted: $emailWhenInvitationAccepted, emailWhenMessageReceivedOffline: $emailWhenMessageReceivedOffline) {
          ...on Session {
          user {
            id
            firstname
            username
            email
            preferences {
              emailWhenInvitationAccepted
              emailWhenMessageReceivedOffline
            }
            createdAt
            deleted
          }
        }
        ...on UserInputError {
          error {
            key
            message
            fields
          }
        }
      }
    }`

    it('should allow a new user to register', async () => {
      const variables = {
        email: 'shasha.velours@wow.com',
        username: 'shasha',
        firstname: 'Sasha',
        password: 'Str0ngPassword@1',
        emailWhenInvitationAccepted: true,
        emailWhenMessageReceivedOffline: false,
      }
      const {
        data: {
          session: { user },
        },
        error,
      } = await client.request({ query, variables })
      expect(error).toBeUndefined()
      expect(user.id).not.toBe(null)
      expect(user.email).toBe(variables.email)
      expect(user.username).toBe(variables.username)
      expect(user.firstname).toBe(variables.firstname)
      expect(user.preferences.emailWhenInvitationAccepted).toBe(
        variables.emailWhenInvitationAccepted
      )
      expect(user.preferences.emailWhenMessageReceivedOffline).toBe(
        variables.emailWhenMessageReceivedOffline
      )
    })
  })

  describe('updateUser mutation', () => {
    const query = `
      mutation updateUser($update: UserUpdate!) {
        updatedUser: updateUser (update: $update) {
          ...on User {
            id
            firstname
            username
            email
            createdAt
            deleted
          }
          ...on UserInputError {
            error {
              key
              fields
              message
            }
          }
        }
      }
    `

    it('should prevent a user to update their username or email to one already used', async () => {
      const client = await new GraphqlClient().createSession({
        email: activeUser.email,
        password: activeUser.password,
      })
      // email
      const emailUpdate = await client.request({
        query,
        variables: { update: { email: 'alyssa.edwards@potus.com' } },
      })
      expect(emailUpdate.error).toBeUndefined()
      const emailError = emailUpdate.data.updatedUser.error
      expect(emailError.fields[0]).toBe('email')
      expect(emailError.key).toBe('USER_EMAIL_ALREADY_EXISTS')
      // username
      const usernameUpdate = await client.request({
        query,
        variables: { update: { username: 'alyssa' } },
      })
      expect(usernameUpdate.error).toBeUndefined()
      const usernameError = usernameUpdate.data.updatedUser.error
      expect(usernameError.fields[0]).toBe('username')
      expect(usernameError.key).toBe('USERNAME_ALREADY_EXISTS')
    })

    it('should allow a logged in user to update their information', async () => {
      const client = await new GraphqlClient().createSession({
        email: activeUser.email,
        password: activeUser.password,
      })
      const variables = {
        update: {
          email: 'yolo@yolo.com',
          firstname: 'Patrick',
          username: 'patty',
        },
      }
      const {
        errors,
        data: {
          updatedUser: { email, firstname, createdAt, deleted },
        },
      } = await client.request({ query, variables })
      if (errors) throw errors
      expect(email).toBe(variables.update.email)
      expect(firstname).toBe(variables.update.firstname)
      expect(typeof createdAt).toBe('string')
      expect(typeof deleted).toBe('boolean')
    })
  })
})
