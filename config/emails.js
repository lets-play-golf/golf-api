const {
  EMAILS: { nodemailerTransport },
} = require('./config')

module.exports = {
  nodemailerTransport,
}
