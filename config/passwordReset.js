module.exports = {
  passwordResetRequestedEmail: {
    template: 'PasswordResetRequested',
    subject: 'Resetting your password',
  },
}
