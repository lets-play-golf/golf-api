const {
  GRAPHQL: { APOLLO_KEY, APOLLO_GRAPH_VARIANT, APOLLO_SCHEMA_REPORTING },
} = require('./config')

module.exports = {
  tracing: true,
  playground: true,
  introspection: true,
  APOLLO_KEY,
  APOLLO_GRAPH_VARIANT,
  APOLLO_SCHEMA_REPORTING,
}
