const {
  CORS,
  GRAPHQL: { protocol, host, endpoint },
  PORT,
} = require('./config')

module.exports = {
  graphql: {
    protocol,
    host,
    port: PORT,
    endpoint,
    url: `${protocol}://${host}:${PORT}/${endpoint}`,
    baseUrl: '',
    cors: {
      credentials: true,
      origin: CORS.origin.map((str) => new RegExp(str)) || false,
    },
  },
}
