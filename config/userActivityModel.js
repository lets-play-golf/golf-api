const {
  USER_ACTIVITY: { notifyAdmin },
} = require('./config')

module.exports = {
  notifyAdmin,
  accountLock: {
    allowedFailedAttempts: 3,
    timespan: 1000 * 60 * 15,
  },
  passwordResetRequested: {
    timespan: 1000 * 60 * 15,
  },
}
