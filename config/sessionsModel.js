const {
  CORS,
  TOKEN: { encryptionKey, lifespanInHours },
} = require('./config')

module.exports = {
  token: {
    encryptionKey,
  },
  cookie: {
    name: 'token',
    lifespan: lifespanInHours * 60 * 60 * 1000,
    options: {
      credentials: true,
      origin: CORS.origin.map((str) => new RegExp(str)) || false,
    },
  },
}
