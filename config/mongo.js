const {
  // MONGO has configs for `prod`, `local` and `test`
  MONGO,
} = require('./config')

// When starting without a NODE_ENV, connect to production DB
const isProduction = !['local', 'test'].includes(process.env.NODE_ENV)

// Otherwise, serve 'local' or 'test' config
const environment = isProduction ? 'prod' : process.env.NODE_ENV

const config = MONGO[environment]

if (!config)
  throw new Error(`No mongo config found for environment ${environment}`)

module.exports = config
