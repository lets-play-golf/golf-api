module.exports = {
  saltRounds: 10,
  passwordRegex: /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/,
  usernameRegex: /^[a-zA-Z0-9-]{3,8}$/,
  emailRegex: /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/,
  createUserEmail: {
    template: 'CreateUser',
    subject: 'Welcome to lets-play-golf',
  },
  updateUserEmail: {
    template: 'UpdateUser',
    subject: 'Your account information were updated',
  },
}
