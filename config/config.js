/**
 * Reads `app.env.json` config file at the root of the project
 * Used for local and render.com production environment
 * @returns {Object} config
 */
const readConfigFile = () => {
  const configFile = require('../app.env.json')
  if (!configFile)
    throw Error('Local config file app.env.json could not be found')
  return configFile
}

const isOnHeroku = process.env.NODE_ENV === 'production'

/**
 * On heroku: parse Json object from process.env object which includes env variables
 * @returns {Object} config
 */
const herokuConfig = () => {
  return Object.entries(process.env).reduce((acc, [key, value]) => {
    try {
      if (typeof value === 'string' && value.startsWith('{'))
        acc[key] = JSON.parse(value)
      else acc[key] = value
    } catch (e) {
      console.log(
        `Heroku config var error for key ${key}, value ${value}, error`,
        e
      )
    }
    return acc
  }, {})
}

const isOnRenderDotCom = !!process.env.RENDER_SERVICE_ID

/**
 * On render.com: Read config file from app.env and override port number with the one provided in process.env.port
 * @returns {Object} config
 */
const renderDotComConfig = () => {
  const config = readConfigFile()
  // Override port with the render dot com port
  config.PORT = process.env.PORT
  return config
}

const isOnflyDotIo = !!process.env.FLY_REGION

/**
 * On fly.io: Read config file from secrets
 * @returns {Object} config
 */
const flyDotIoConfig = () => {
  try {
    const secrets = process.env.SECRETS
    return JSON.parse(secrets)
  } catch (e) {
    throw new Error('Could not parse secret named `SECRETS` in fly.io app')
  }
}

const config = (() => {
  if (isOnflyDotIo) return flyDotIoConfig()
  if (isOnRenderDotCom) return renderDotComConfig()
  if (isOnHeroku) return herokuConfig()
  return readConfigFile()
})()

module.exports = config
