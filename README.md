# Lets play golf - API

Lets-play-golf is a web application where a community of players can challenge each other playing pub golf, a popular playing cards game.
[Join us now and start playing!](https://lets-play-golf.herokuapp.com/)

## Overview

This projects is the gateway API for [lets-play-golf](https://lets-play-golf.com/).
See [golf-ui](https://gitlab.com/lets-play-golf) for the UI project.

### Architecture

The backend runs on an Express server acting as a single gateway with a graphql interface.
User data is stored on a sharded MongoDB cluster.

### Domain

User data is stored in mongoDB collections:

- users: account details
- games: card deck and scores
- chats: private and public chats between users
- user-activity: mostly used to track password reset requests and support magic links.

## Getting started

Clone the project and install code dependencies

```shell
yarn
```

### Add config

Get the secret config file from the config project and add it in the root.

### Local DB dependency

In order to run the gateway and/or the tests locally, create and start up the MongoDB dependency by running:

```shell
docker-compose up -d
```

### Start up server

Startup script connects to production DB by default:

```shell
yarn start
```

You can also connect to local:

```shell
yarn start:local
```

And test DB:

```shell
yarn start:test
```

## Testing

This project relies extensively on integration tests for all queries and mutations.
There are 2 test configs: test and local, each connecting to different DBs hosted locally.

### Integration Tests

This script starts up an instance of the server connected to test DB and then executes the tests.

```shell
yarn test-integration
```

When the server is already started, you can use the script with a suffix to executes the tests only, specifying the DB config:

```shell
yarn test-integration:local
```

### Unit tests

This script executes unit test in isolation and do not require the server to be started

```shell
yarn test
```

## Hosting

To simplify the deployment process in a single app, the API server both handles graphql requests and acts as a webserver to serve client assets.
When the project is deployed, the gateway accepts requests on a port allocated by heroku.

## Debug

To debug in visual code, run gateway with the `debug` config profile.
You can also start up the gateway by running `yarn debug` and pick the process using the `pick process` config profile.

## Scripts

Custom shell scripts are available in the `scripts` folder.

### Get started - install

```shell
yarn
```

### Start server

Gateway will start up against port 4001

```shell
yarn start
```

### Linting

To fix linting errors

```shell
yarn fix-lint
```

### Testing

Starts up an instance of the gateway and runs tests against the mongo db `db-test`

```shell
yarn test
```

### Heroku scripts - prefixed with a `h` in package.json.

These shell scripts were coded to automate the build and deploy process to heroku via heroku git.
Note: for these scripts to work, you need to install the heroku CLI and run the login command.

```shell
heroku login
```

- To get started, run the setup script. It creates the folder `lets-play-golf-deploy` one level up and synchronises it with heroku git so it is ready to deploy code.
- Then you can use the deploy script. It downloads the specified branch from git for both `golf-api` and `golf-ui`, runs the production build process for the UI and uploads the code to heroku git. Heroku will pick up on it and run its build and restart process. Both UI and API are served from a single heroku app to simplify the deployment process.

#### Setup

Sets up a folder one level up and synchronises it with heroku git so it is ready to deploy code to the heroku app.

```shell
yarn hsetup
```

#### Deploy

Clones the specified branch (or `master` by default) from `golf-api` and `golf-ui` projects, runs ui production build and deploys ui and api to the heroku app.

```shell
yarn hdeploy <optional_branch_name>
```

#### Logs

When Heroku has successfully deployed the code, run this command to see restart logs.

```shell
yarn hlogs
```

#### Restart

Restarts the heroku app

```shell
yarn hrestart
```

## TODO

Improve coverage:
https://stackoverflow.com/questions/58597960/collecting-coverage-reports-in-jest-with-integration-tests
